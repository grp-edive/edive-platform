# eDIVE Platform

## About
eDIVE is a software platform for education in collaborative immersive virtual environment based on Unity game engine. The main purposes of eDIVE are:
1. Simplify the process of creating educational scenarios to developers.
2. Allow teachers and students to conduct education in custom-made virtual environments with tailored functionality.
3. Help researchers from various fields including computer science, education or psychology to conduct their research related to VR, online collaboration, education and more.

## Documentation
See [eDIVE Documentation](https://grp-edive.pages.fi.muni.cz/edive-platform).


## Development Roadmap
 * 1st August 2023 - Release of "Core" module of eDIVE platform
 * Q3, Q4 2023 - piecewise releasing of additional modules

## Version History
* v1.0 (01-Aug-2023) - initial public release.

## License
Open-source version of eDIVE is licensed under the [LGPL-3.0](./LICENSE.md).

#### Dual-license

You can use this open-source edition of eDIVE to create open-source
softwares, under the GNU LGPL-3.0 license or a similarly recognized
open-source license.

It is also possible to develop commercial software using eDIVE but
to do so you must buy a commercial license for eDIVE Commercial version.
If you buy a commercial version of eDIVE you can sell your software
for any price you like.

If you use eDIVE open-source version, there are certain licensing
conditions that the LGPL imposes on you, to ensure that your users enjoy
the freedoms guaranteed by the LGPL. Users are entitle to:

- Run your software for any purpose.
- Obtain and study your software's source code, and adapt it to their needs.
- Redistribute your software and its source code to others (under the same terms).
- Improve or modify your software, and release these changes to the public (hopefully via the gitlab repository)

These freedoms apply to all the source code for all the modules your
software is based on, regardless of whether they have been written
by you or by others.

## Contributions
See [Contributing]((https://grp-edive.pages.fi.muni.cz/edive/manual/Contributing.html)) page of documentation.

## Acknowledgement
The eDIVE platform is developed in the research project of the Technology Agency of the Czech Republic (TL03000346 Education in Collaborative Immersive Virtual Reality) at the Department of Information and Library Studies and Department of Visual Computing, Masaryk University.

## Disclaimer
This repository is not sponsored by or affiliated with Unity Technologies or its affiliates. “Unity” is a trademark or registered trademark of Unity Technologies or its affiliates in the U.S. and elsewhere.
