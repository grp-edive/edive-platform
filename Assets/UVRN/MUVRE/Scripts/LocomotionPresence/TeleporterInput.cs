using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace MUVRE.Locomotion
{
    [RequireComponent(typeof(XRRayInteractor))]
    public class TeleporterInput : MonoBehaviour
    {
        [SerializeField]
        private InputActionReference selectReference;
        [SerializeField]
        private InputActionReference activateReference;
        [SerializeField]
        private InputActionReference cancelReference;

        private XRRayInteractor rayInteractor;

        private bool selected = false;

        private void Awake()
        {
            rayInteractor = GetComponent<XRRayInteractor>();

            if ((rayInteractor.xrController as ActionBasedController).selectAction.reference != activateReference)
            {
                Debug.LogError($"The {nameof(TeleporterInput)} uses a different action to interact with the teleportation layer.");
            }
        }

        private void OnEnable()
        {
            selectReference.action.performed += OnSelect;
            activateReference.action.performed += OnActivate;
            cancelReference.action.performed += OnCancel;
        }
        private void OnDisable()
        {
            selectReference.action.performed -= OnSelect;
            activateReference.action.performed -= OnActivate;
            cancelReference.action.performed -= OnCancel;
        }


        private void OnSelect(InputAction.CallbackContext context)
        {
            selected = true;
        }

        private void OnActivate(InputAction.CallbackContext context)
        {
            // No idea why this works here I would suppose oposite behaviour (do this in cancel) but no...this works as expected
            rayInteractor.allowSelect = false;
            Stop();
        }

        private void OnCancel(InputAction.CallbackContext context)
        {
            Stop();
        }

        private void Stop()
        {
            selected = false;
        }

        private void LateUpdate()
        {
            // late update after all interactors are processed
            ApplyState();

            rayInteractor.allowSelect = true;
        }

        private void ApplyState()
        {
            if (selected != rayInteractor.enabled)
            {
                // disable or enable ray based on the input
                rayInteractor.enabled = selected;
            }
        }
    }
}
