using UnityEngine.XR.Interaction.Toolkit;

namespace MUVRE.Locomotion
{
    public class MUVRE_SnapTurnProvider : ActionBasedSnapTurnProvider
    {
        public void EnableLeftHand()
        {
            leftHandSnapTurnAction.action.Enable();
        }

        public void DisableLeftHand()
        {
            leftHandSnapTurnAction.action.Disable();
        }
        public void EnableRightHand()
        {
            rightHandSnapTurnAction.action.Enable();
        }
        public void DisableRightHand()
        {
            rightHandSnapTurnAction.action.Disable();
        }
    }
}
