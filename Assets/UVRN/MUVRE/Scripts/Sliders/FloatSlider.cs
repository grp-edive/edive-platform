﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.Events;

namespace MUVRE
{ 
    /// <summary>
    /// Component providing the float slider functionality
    /// </summary>
    public class FloatSlider : SliderBase
    {
        [Tooltip("The events invoked when interacting with the slider")]
        [SerializeField] private UnityEventFloat onMove;
        [Tooltip("The minimum value the slider can reach")]
        [SerializeField] protected float minValue = 0;
        [Tooltip("The maximum value the slider can reach")]
        [SerializeField] protected float maxValue = 100;
        [Tooltip("The default value of the slider")]
        [SerializeField] protected float defaultValue = 0;

        /// <summary>
        /// The current value of the slider
        /// </summary>
        public float CurrentValue {
            set => SetValue(value); 
            get => currentValue; 
        }

        private float currentValue;

        protected override void Start()
        {
            base.Start();
            SetValue(defaultValue);
        }

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            if (slideInteractor)
            {
                float newPosition;
                if (slideInteractor is XRRayInteractor)             
                   newPosition = Mathf.Clamp(Utils.GetLocalPosition(attachPoint.transform.position, transform).x , xMin, xMax);                               
                else    
                    newPosition = Mathf.Clamp(Utils.GetLocalPosition(slideInteractor.transform.position, transform).x + deltaX , xMin, xMax);             
                
                SetXPosition(newPosition);
                currentValue = Utils.Map(newPosition, xMin, xMax, minValue, maxValue);
                InvokeEvent();
            }
        }

        /// <summary>
        /// Invokes the associated event
        /// </summary>
        public override void InvokeEvent() => onMove.Invoke(currentValue);


        /// <summary>
        /// Sets a new value to the slider
        /// </summary>
        /// <param name="value">The new value</param>
        public void SetValue(float value)
        {
            currentValue = Mathf.Clamp(value, minValue, maxValue);
            SetXPosition(Utils.Map(currentValue, minValue, maxValue, xMin, xMax));      
        }
    }
}