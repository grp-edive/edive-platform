﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
/// Base class of all sliders
/// </summary>
namespace MUVRE
{
    public abstract class SliderBase : XRBaseInteractable
    {
        [Tooltip("The rail of the pivot transform")]
        public Transform railPivot;
        public float size = 1;
        [Tooltip("Overlap of the rail")]
        [SerializeField] protected float overlap;
        [Tooltip("The slider handle")]
        [SerializeField] protected Transform buttonTransform;
      
        protected float xMin = 0.0f;
        protected float xMax = 0.0f;
        protected float deltaX = 0.0f;
        protected XRBaseInteractor slideInteractor = null;
        protected XRRayInteractor rayInteractor;
        protected Transform attachPoint;
        protected override void Awake()
        {
            base.Awake();
            if (!buttonTransform)
                buttonTransform = transform.GetComponentInChildren<Rigidbody>().transform;
        }

        protected virtual void Start() => SetMinMax();

        private void StartMove(SelectEnterEventArgs args)
        {
            slideInteractor = args.interactor;
            if (args.interactor is XRDirectInteractor)
                deltaX = buttonTransform.transform.localPosition.x - Utils.GetLocalPosition(slideInteractor.transform.position, transform).x;
            else if (args.interactor is XRRayInteractor)
            {
                rayInteractor = (XRRayInteractor)slideInteractor;
                attachPoint = rayInteractor.transform.Find("RayAttachPoint");
                if (!attachPoint)
                    attachPoint = new GameObject("RayAttachPoint").transform;
                attachPoint.parent = rayInteractor.transform;
                attachPoint.position = buttonTransform.position;
                attachPoint.rotation = buttonTransform.rotation;
            }
        }

        private void EndMove(SelectExitEventArgs args) => slideInteractor = null;

        protected void SetMinMax()
        {
            xMin = buttonTransform.transform.localPosition.x;
            xMax = buttonTransform.transform.localPosition.x + railPivot.transform.localScale.x - 1;

            Vector3 scale = railPivot.transform.localScale;
            scale.x += overlap * 2;
            railPivot.transform.localScale = scale;

            Vector3 position = railPivot.transform.localPosition;
            position.x -= overlap;
            railPivot.transform.localPosition = position;
        }

        protected void SetXPosition(float position)
        {
            Vector3 newPosition = buttonTransform.transform.localPosition;
            newPosition.x = position;
            buttonTransform.transform.localPosition = newPosition;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            selectEntered.AddListener(StartMove);
            selectExited.AddListener(EndMove);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            selectEntered.RemoveListener(StartMove);
            selectExited.RemoveListener(EndMove);
        }

       public virtual void InvokeEvent() { }
    }
}
