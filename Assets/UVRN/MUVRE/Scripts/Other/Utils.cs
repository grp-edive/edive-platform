﻿using UnityEngine;

namespace MUVRE
{
    /// <summary>
    /// A class that provides various utilities
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// Maps a number from one range to another
        /// </summary>
        /// <param name="value">The input number</param>
        /// <param name="from1">The lower bound of the input range</param>
        /// <param name="to1">The upper bound of the input range</param>
        /// <param name="from2">The lower bound of the new range</param>
        /// <param name="to2">The upper bound of the new range</param>
        public static float Map(float value, float from1, float to1, float from2, float to2) => (value - from1) / (to1 - from1) * (to2 - from2) + from2;

        /// <summary>
        /// Transforms a position to local space of given transform
        /// </summary>
        /// <param name="position">The position to be transformed</param>
        /// <param name="transform">The transform to whose local space the position is transformed to</param>
        public static Vector3 GetLocalPosition(Vector3 position, Transform transform) => transform.InverseTransformPoint(position);
    }
}