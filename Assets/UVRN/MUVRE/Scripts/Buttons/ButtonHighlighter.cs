﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace MUVRE
{
    /// <summary>
    /// Component used for button highlighting
    /// </summary>
    [RequireComponent(typeof(XRBaseInteractable))]
    public class ButtonHighlighter : MonoBehaviour
    {
        [Header("Materials")]
        [Tooltip("The material the button should be set to while being hovered")]
        [SerializeField] private Material hoverMaterial = null;
        [Tooltip("The material the button should be set to if toggled on")]
        [SerializeField] private Material toggleOnMaterial = null;
        [Tooltip("The material the button should be set to when disabled")]
        [SerializeField] private Material disabledMaterial = null;
        [Tooltip("The mesh renderer of the button object")]
        [SerializeField] protected MeshRenderer buttonRenderer = null;
        private Material originalMaterial = null;

        protected XRBaseInteractable interactable;

        private void Start() => SetDisableMaterial();

        protected void Awake()
        {       
            originalMaterial = buttonRenderer.material;
            interactable = GetComponent<XRBaseInteractable>();        
        }

        protected void OnEnable()
        {
            interactable.hoverEntered.AddListener(SetHoverMaterial);
            interactable.hoverExited.AddListener(SetIdleMaterial);

            interactable.registered += _ => SetDisableMaterial();
            interactable.unregistered += _ => SetDisableMaterial();
        }

        protected void OnDisable()
        {
            interactable.hoverEntered.RemoveListener(SetHoverMaterial);
            interactable.hoverExited.RemoveListener(SetIdleMaterial);

            interactable.registered -= _ => SetDisableMaterial();
            interactable.unregistered -= _ => SetDisableMaterial();
        }

        private void SetHoverMaterial(HoverEnterEventArgs args)
        {
            if (hoverMaterial )
                buttonRenderer.material = hoverMaterial;
        }

        private void SetDisableMaterial()
        {
            if (disabledMaterial  && !interactable.enabled)
                buttonRenderer.material = disabledMaterial;
            else
                buttonRenderer.material = originalMaterial;
        }

        private void SetIdleMaterial(HoverExitEventArgs args)
        {
            if (GetComponent<PhysicalButton>() && GetComponent<PhysicalButton>().Status && toggleOnMaterial)
                buttonRenderer.material = toggleOnMaterial;
            else
                buttonRenderer.material = originalMaterial;
        }

    }
}