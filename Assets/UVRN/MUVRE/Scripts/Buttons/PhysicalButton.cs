﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace MUVRE
{
    /// <summary>
    /// Base class for all buttons
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public abstract class PhysicalButton : XRBaseInteractable
    {
        [Tooltip("The events invoked after pressing the button.")]
        [SerializeField] protected UnityEvent onPress = null;
        [Tooltip("The Audio Clip that plays after pressing the button.")] 
        [SerializeField] protected AudioClip clickSound;
        [Tooltip("The duration of the haptic feedback performed by the controller that pressed the button.")] 
        [SerializeField] protected float hapticDuration;
        [Tooltip("The amplitude of the haptic feedback performed by the controller that pressed the button.")]
        [SerializeField] protected float hapticAmplitude;

        protected float yMin = 0.0f;
        protected float yMax = 0.0f;
        protected XRBaseInteractor interactor = null;
        protected Transform buttonTransform;
        private AudioSource _source;
        protected bool on = false;

        /// <summary>
        /// Returns whether the button is currently toggled on
        /// </summary>
        public bool Status => on;

        protected override void Awake() 
        {
            base.Awake();
            if (!buttonTransform)
                buttonTransform = transform.GetComponentInChildren<Rigidbody>().transform;
            _source = GetComponent<AudioSource>();
        }

        /// <summary>
        /// Invokes the button event, plays the Audio Clip and performs the haptic feedback
        /// </summary>
        public virtual void InvokeButtonEvent(UnityEvent buttonEvent, float amp, float dur, AudioClip clickSound)
        {
            interactor?.GetComponent<XRBaseController>().SendHapticImpulse(amp, dur);
            if (clickSound) _source.PlayOneShot(clickSound);
            buttonEvent?.Invoke();
        }

    }
}