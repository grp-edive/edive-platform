﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEditor.XR.Interaction.Toolkit;

namespace MUVRE
{   
    [CustomEditor(typeof(SliderBase), true)]
    public class SliderEditor : XRBaseInteractableEditor
    {    
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            SliderBase slider = (SliderBase)target;
            if (slider?.railPivot) slider.railPivot.localScale = new Vector3(slider.size, slider.railPivot.localScale.y, slider.railPivot.localScale.z);        
        }
    }  
}