﻿// Created by Vojtech Bruza
using Mirror;
using UnityEngine;


namespace UVRN.Player
{
    public class UVRN_TrackedModelNetworkTransform : MonoBehaviour
    {
        public Transform movementSourceToTrack;

        private void FixedUpdate()
        {
            if (movementSourceToTrack)
            {
                transform.position = movementSourceToTrack.position;
                transform.rotation = movementSourceToTrack.rotation;
            }
        }
    }
}
