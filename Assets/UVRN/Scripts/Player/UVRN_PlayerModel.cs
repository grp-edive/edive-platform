﻿// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace UVRN.Player
{
    // TODO move to MUVRE?

    /// <summary>
    /// Local player visual (avatar). Has references to hands models and other visuals representing the player for everyone.
    /// </summary>
    [RequireComponent(typeof(XRBaseInteractable))]
    public class UVRN_PlayerModel : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("Used for color indication")]
        private Renderer[] renderers;

        public UVRN_TrackedModelNetworkTransform headModel;
        public UVRN_TrackedModelNetworkTransform leftHandModel;
        public UVRN_TrackedModelNetworkTransform rightHandModel;
        public GameObject leftHandControllerVisual;
        public GameObject rightHandControllerVisual;

        // TODO remove? Unnecessary to have it here
        public UVRN_LinePointer leftHandLaserPointer;
        public UVRN_LinePointer rightHandLaserPointer;

        /// <summary>
        /// visible when grabbing objects etc
        /// </summary>
        public UVRN_LinePointer leftHandLinePointer;
        public UVRN_LinePointer rightHandLinePointer;

        /// <summary>
        /// To be able to click on a player.
        /// </summary>
        private XRSimpleInteractable interactable;

        public Transform isSpeakingIcon;

        [SerializeField]
        private Transform muteIcon;

        [SerializeField]
        private TextMesh playerNameText;

        [SerializeField]
        private TextMesh playerRoleText;

        [SerializeField]
        private Transform FloatingHeadUI;

        public UnityEvent PlayerClicked = new UnityEvent();

        public bool IsLocalPlayer = false;

        public Color Color
        {
            get
            {
                if (renderers.Length > 0)
                {
                    return renderers[0].material.color;
                }
                else
                {
                    return Color.black;
                }
            }
            set
            {
                if (renderers.Length > 0)
                {
                    foreach (var rend in renderers)
                    {
                        var mat = new Material(rend.material);
                        mat.color = value;
                        rend.material = mat;
                    }
                }
                else
                {
                    Debug.LogError("Could not set color for player model. No renderers set up. You need to assign the renderers in the editor first.");
                }
            }
        }

        private void OnEnable()
        {
            interactable.activated.AddListener(OnActivate);
        }
        private void OnDisable()
        {
            interactable.activated.RemoveListener(OnActivate);
        }

        private void Awake()
        {
            interactable = GetComponent<XRSimpleInteractable>();
            muteIcon.gameObject.SetActive(false);
        }

        private void Start()
        {
            if (renderers.Length <= 0)
            {
                Debug.LogWarning("No renderers set for the player model.");
            }
        }

        private void Update()
        {
            // turn all other player names to look at the local camera
            if (!IsLocalPlayer) FloatingHeadUI.LookAt(Camera.main?.transform);
        }

        public void SetColor(Color color)
        {
            playerNameText.color = color;
            playerRoleText.color = color;
            Color = color;
        }
        public void SetUsername(string name)
        {
            playerNameText.text = name;
        }

        public void ToggleMute(bool muted)
        {
            muteIcon.gameObject.SetActive(muted);
        }

        public void SetRole(string role)
        {
            playerRoleText.text = role;
        }
        private void OnActivate(ActivateEventArgs args)
        {
            PlayerClicked.Invoke();
        }
    }
}
