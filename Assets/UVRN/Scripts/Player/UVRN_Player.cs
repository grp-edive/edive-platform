﻿// Created by Vojtech Bruza
using Mirror;
using System.Collections.Generic;
using UnityEngine;
using UVRN.Player.Logging;
using UVRN.VoiceChat;

namespace UVRN.Player
{
    /// <summary>
    /// Class responsible for the player model networking.
    /// </summary>
    public class UVRN_Player : NetworkBehaviour
    {
        private UVRN_XRManager m_uvrn_XRManager;

        private UVRN_NetworkManager m_netManager;
        private UVRN_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        public UVRN_PlayerModel PlayerModel;

        [Tooltip("Hide this for the local player")]
        [SerializeField]
        private Transform playerBodyVisualization;

        /// <summary>
        /// How many times per second update the spatial audio
        /// If less or equal to 0, does not update at all
        /// </summary>
        private static float audioPositionUpdateFrequency = 5f;

        public bool Muted { get; private set; }

        /// <summary>
        /// List of all connected players. Available on each client. (alternative to list of connections on the server)
        /// Needed e.g. for the voice chat functionality (find player on the client just by his ID).
        /// Includes the local player.
        /// </summary>
        public static readonly Dictionary<uint, UVRN_Player> Client_ConnectedPlayers = new Dictionary<uint, UVRN_Player>();

        public static UnityEventUint Client_OnPlayerJoined = new UnityEventUint();
        public static UnityEventUint Client_OnPlayerLeft = new UnityEventUint();


        [SyncVar(hook = nameof(HandleColorChanged))]
        private Color color = Color.white;

        [SyncVar(hook = nameof(HandleUsernameChanged))]
        private string username;

        [SyncVar(hook = nameof(HandleRoleChanged))]
        private string role;

        [SyncVar(hook = nameof(HandleVisibleChanged))]
        private bool modelVisible = true;

        public string Username => username;

        public string Role => role;

        public Color Color => color;

        public uint ID => netIdentity.netId;

        public static UVRN_Player LocalPlayer => NetworkClient.localPlayer?.GetComponent<UVRN_Player>();

        public void HandleUsernameChanged(string _old, string _new)
        {
            // Trigger the syncvar sync - invokes this method on clients when called on the server (redundant on the server, but best)
            username = _new;

            // Update this on both server and clients
            PlayerModel.SetUsername(username);
        }

        public void HandleRoleChanged(string _old, string _new)
        {
            // Trigger the syncvar sync - invokes this method on clients when called on the server (redundant on the server, but best)
            role = _new;

            // Update this on both server and clients
            PlayerModel.SetRole(role);
        }

        public void HandleVisibleChanged(bool _old, bool _new)
        {
            // Trigger the syncvar sync - invokes this method on clients when called on the server (redundant on the server, but best)
            modelVisible = _new;

            // Update this on both server and clients
            PlayerModel.gameObject.SetActive(_new);
        }

        public void HandleColorChanged(Color _old, Color _new)
        {
            // Trigger the syncvar sync - invokes this method on clients when called on the server (redundant on the server, but best)
            color = _new;

            // Update this on both server and clients
            PlayerModel.SetColor(color);
            PlayerModel.leftHandLinePointer.SetColor(color);
            PlayerModel.rightHandLinePointer.SetColor(color);
        }

        public override void OnStartServer()
        {
            // TODO move this somewhere else to get rid of the dependency, but have to rewrite some stuff first to ensure the correct order of execution (logger after network id)
            GetComponent<UVRN_PlayerLogger>()?.Init(this);
        }

        public override void OnStartClient()
        {
            uint id = ID;
            Client_ConnectedPlayers[id] = this;
            Debug.Log("New client joined with id " + id);
            Client_OnPlayerJoined.Invoke(id);
        }

        public override void OnStopClient()
        {
            uint id = ID;
            Client_ConnectedPlayers.Remove(id);
            Debug.Log("Client leaving with id " + id);
            Client_OnPlayerLeft.Invoke(id);
        }

        public override void OnStartLocalPlayer()
        {
            // TODO how to ensure init?
            if (!PlayerModel) Debug.LogError("Model not yet initialized");

            InitTracking();

            // TODO move this stuff to the player model initialization (decompose):
            PlayerModel.IsLocalPlayer = true;

            // hide the local player body/head
            playerBodyVisualization.gameObject.SetActive(false);

            // hide the local controllers (TODO should be in the xr rig)
            PlayerModel.leftHandControllerVisual.SetActive(false);
            PlayerModel.rightHandControllerVisual.SetActive(false);

            // hide the local line pointers
            PlayerModel.leftHandLinePointer.gameObject.SetActive(false);
            PlayerModel.rightHandLinePointer.gameObject.SetActive(false);

            if (audioPositionUpdateFrequency > 0) InvokeRepeating(nameof(UpdateAudioPosition), 0, 1f / audioPositionUpdateFrequency);

            netManager.VoiceChatManager.StartVoiceChat(ID.ToString());
        }

        private void InitTracking()
        {
            if (UVRN_TrackeableControls.Instance != null)
            {
                // set spawned models to track instantiated XR controllers and player representation
                PlayerModel.headModel.movementSourceToTrack = UVRN_TrackeableControls.Instance.GetHead();
                PlayerModel.leftHandModel.movementSourceToTrack = UVRN_TrackeableControls.Instance.GetLeftHand();
                PlayerModel.rightHandModel.movementSourceToTrack = UVRN_TrackeableControls.Instance.GetRightHand();
            }
            else
            {
                Debug.Log("No able to find trackable component. There are no controls...");
            }
        }

        private void Awake()
        {
            if (!PlayerModel?.isSpeakingIcon) Debug.LogError("Voice vis null");
        }

        private void OnEnable()
        {
            PlayerModel.PlayerClicked.AddListener(OnPlayerClicked);
        }

        private void OnDisable()
        {
            PlayerModel.PlayerClicked.RemoveListener(OnPlayerClicked);
        }

        private void OnPlayerClicked()
        {
            Debug.Log("Trying to mute: " + ID);
            // if local player clicked on this user, then mute him for the local user
            netManager.VoiceChatManager.ToggleMuteOtherUser(ID.ToString());
        }

        private void UpdateAudioPosition()
        {
            // TODO cache the position and update only if changed a bit
            netManager.VoiceChatManager?.SetWorldPosition(PlayerModel.headModel.transform.position, PlayerModel.headModel.transform.forward, PlayerModel.headModel.transform.up);
        }

        public void Mute(bool muted)
        {
            if (Muted != muted && !isLocalPlayer)
            {
                Muted = muted;
                PlayerModel.ToggleMute(muted);
            }
        }

        /// <summary>
        /// Done according to https://mirror-networking.gitbook.io/docs/guides/gameobjects/custom-character-spawning
        /// It would be nicer to use something instead of all the syncvars, but this is the best for now
        /// </summary>
        /// <param name="profile"></param>
        [Server]
        public void ApplyProfile(UVRN_PlayerProfile profile)
        {
            HandleColorChanged(color, profile.color);
            HandleUsernameChanged(username, profile.username);
            HandleRoleChanged(role, profile.role);
            HandleVisibleChanged(modelVisible, profile.visibleAvatar);
        }

        [Command]
        public void Cmd_ToggleLeftLaser(bool active)
        {
            PlayerModel.leftHandLaserPointer.gameObject.SetActive(active);
            Rpc_ToggleLeftLaser(active);
        }

        [Command]
        public void Cmd_ToggleRightLaser(bool active)
        {
            PlayerModel.rightHandLaserPointer.gameObject.SetActive(active);
            Rpc_ToggleRightLaser(active);
        }

        [ClientRpc]
        public void Rpc_ToggleLeftLaser(bool active)
        {
            PlayerModel.leftHandLaserPointer.gameObject.SetActive(active);
        }

        [ClientRpc]
        public void Rpc_ToggleRightLaser(bool active)
        {
            PlayerModel.rightHandLaserPointer.gameObject.SetActive(active);
        }

        public bool IsSpeaking
        {
            get
            {
                return netManager.VoiceChatManager.IsSpeaking(ID.ToString());
            }
        }

        private void Update()
        {
            if(PlayerModel.isSpeakingIcon != null && ID > 0 && netManager != null)
            {
                bool speaks = IsSpeaking;
                PlayerModel.isSpeakingIcon.gameObject.SetActive(speaks);
            }
        }
    }
}
