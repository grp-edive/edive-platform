﻿// Created by Vojtech Bruza
using Mirror;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace UVRN.Player
{
    public class UVRN_XRManager : UVRN_TrackeableControls
    {
        #region Net Manager
        private UVRN_NetworkManager m_netManager;
        private UVRN_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }
        #endregion

        [SerializeField]
        private bool dontDestroyOnLoad = false;

        public XRInteractionManager InteractionManager;
        public XRRig Rig;

        [SerializeField]
        private Transform head;
        [SerializeField]
        private Transform leftController;
        [SerializeField]
        private Transform rightController;

        private TeleportationProvider teleportationProvider;
        
        [SerializeField]
        private InputActionReference leftPointerToggleAction;
        [SerializeField]
        private InputActionReference rightPointerToggleAction;
        [SerializeField]
        private InputActionReference menuAction;
        
        public UVRN_MenuManager menuManager;

        //public XRIDefaultInputActions inputActions;

        private void Awake()
        {
            //inputActions = new XRIDefaultInputActions();

            if (dontDestroyOnLoad) DontDestroyOnLoad(this.gameObject);

            teleportationProvider = Rig.GetComponent<TeleportationProvider>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            // TODO move these to separate class:
            leftPointerToggleAction.action.started += OnLeftToggleActionPerformed;
            leftPointerToggleAction.action.canceled += OnLeftToggleActionPerformed;
            rightPointerToggleAction.action.started += OnRightToggleActionPerformed;
            rightPointerToggleAction.action.canceled += OnRightToggleActionPerformed;

            if (menuAction) menuAction.action.performed += MenuAction_performed;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            leftPointerToggleAction.action.started -= OnLeftToggleActionPerformed;
            leftPointerToggleAction.action.canceled -= OnLeftToggleActionPerformed;
            rightPointerToggleAction.action.started -= OnRightToggleActionPerformed;
            rightPointerToggleAction.action.canceled -= OnRightToggleActionPerformed;

            if (menuAction) menuAction.action.performed -= MenuAction_performed;
        }

        private void MenuAction_performed(InputAction.CallbackContext ctx)
        {
            menuManager.gameObject.SetActive(!menuManager.gameObject.activeSelf);
        }

        private void OnLeftToggleActionPerformed(InputAction.CallbackContext ctx)
        {
            if (ctx.started)
            {
                ToggleLeftLaser(true);
            }
            else if (ctx.canceled)
            {
                ToggleLeftLaser(false);
            }
        }

        private void OnRightToggleActionPerformed(InputAction.CallbackContext ctx)
        {
            if (ctx.started)
            {
                ToggleRightLaser(true);
            }
            else if (ctx.canceled)
            {
                ToggleRightLaser(false);
            }
        }

        private void OnRoomLoaded()
        {
            // Is this really happening?
            Debug.Log("Setting up teleportation area for the new room.");
            // TODO do this somewhere smarter to avoid FindObjectsOfType (custom teleportation area script)
            foreach (var area in FindObjectsOfType<TeleportationArea>()) {
                area.teleportationProvider = teleportationProvider;
                area.interactionManager = InteractionManager;
            }
        }

        public override Transform GetHead()
        {
            return head;
        }

        public override Transform GetLeftHand()
        {
            return leftController;
        }

        public override Transform GetRightHand()
        {
            return rightController;
        }

        public void ToggleLeftLaser(bool active)
        {
            if (UVRN_Player.LocalPlayer) UVRN_Player.LocalPlayer.Cmd_ToggleLeftLaser(active);
        }

        public void ToggleRightLaser(bool active)
        {
            if (UVRN_Player.LocalPlayer) UVRN_Player.LocalPlayer.Cmd_ToggleRightLaser(active);
        }
    }
}
