// Created by Vojtech Bruza
using UnityEngine;
using UVRN.Helpers;

namespace UVRN.VoiceChat
{
    public class UVRN_VoiceChatSceneSettings : Singleton<UVRN_VoiceChatSceneSettings>
    {
        public enum AudioFadeModel
        {
            None,
            InverseByDistance,
            LinearByDistance,
            ExponentialByDistance
        }
        public enum ChannelType
        {
            NonPositional,
            Positional
        }

        public ChannelType channelType = ChannelType.NonPositional;

        [Tooltip("The maximum distance away from a speaker that a listener can hear the speaker and receive their text messages. AudibleDistance >= 32 * ConversationalDistance / AudioFadeIntensityByDistance")]
        public int audibleDistance = 64;

        [Tooltip("Controls the range within which a speakerís audio remains at its original volume, and beyond which the loudness of the voice chat starts to fade out when heard.")]
        public int conversationalDistance = 5;

        [Tooltip("Specifies the formula or curve that controls the shape of how the audio fades between the ConversationalDistance and the AudibleDistance.")]
        public float audioFadeIntensityByDistance = 5;

        [Tooltip("Controls the amplitude of the AudioFadeModel curve to make the attenuation of the voice chat loudness more or less extreme.")]
        public AudioFadeModel audioFadeModel = AudioFadeModel.InverseByDistance;
    }
}
