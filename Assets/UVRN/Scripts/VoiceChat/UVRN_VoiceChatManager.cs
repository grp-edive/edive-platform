﻿// Created by Vojtech Bruza
using System;
using UnityEngine;
using UnityEngine.Events;

namespace UVRN.VoiceChat
{
    public class UVRN_VoiceChatManager : MonoBehaviour
    {
        [Serializable]
        public class UnityEventMute : UnityEvent<string, bool> { }

        private UnityEventMute m_onLocalMute = new UnityEventMute();

        public UnityEventMute OnLocalMute => m_onLocalMute;

        public bool MuteMicOnConnect { get; set; }

        public void OnApplicationQuit()
        {
            StopVoiceChat();
        }


        #region Virtual Methods

        public virtual void StartVoiceChat(string id)
        {

        }

        public virtual void StopVoiceChat()
        {

        }

        public virtual bool IsMicMuted()
        {
            return true;
        }

        public virtual void SetMicMuted(bool value)
        {

        }

        public virtual bool IsSpeaking(string iD)
        {
            return false;
        }

        public virtual void ToggleMuteOtherUser(string iD)
        {

        }

        public virtual void SetWorldPosition(Vector3 position, Vector3 forward, Vector3 up)
        {

        }

        // TODO normalize
        // Between -50 and 50 on a log scale
        public virtual void SetMicVolume(int value)
        {

        }

        // TODO normalize
        // Between -50 and 50 on a log scale
        public virtual void SetVoiceChatVolume(int value)
        {

        }

        #endregion
    }
}