﻿// Created by Vojtech Bruza
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UVRN.Helpers;

namespace UVRN.SceneManagement
{
    public struct SceneOperationRequest : NetworkMessage
    {
        public string sceneName;
    }

    public struct SceneOperationInfo : NetworkMessage
    {
        public string sceneName;
        public bool loaded;
    }

    public class UVRN_SceneManager : Singleton<UVRN_SceneManager>
    {
        public bool Initialized { get; private set; }

        public float client_currentLoadingProgress = 0;

        /// <summary>
        /// TODO enum 0 not finished, 1 finished with error, 2 finished
        /// </summary>
        private int server_clientFinishedLoad = 0;

        /// <summary>
        /// Time in seconds to wait for loading a scene before telling the server that it failed.
        /// </summary>
        public float maxSceneLoadingTime = 120;

        private void OnEnable()
        {
            Init();
        }

        private void OnDisable()
        {
            DeInit();
        }

        private void DeInit()
        {
            if (Initialized)
            {
                NetworkClient.UnregisterHandler<SceneOperationRequest>();
                NetworkServer.UnregisterHandler<SceneOperationRequest>();
            }
            else
            {
                Debug.LogWarning("Trying to deinit uninitialized scene manager.");
            }
        }

        private void Init()
        {
            if (!Initialized)
            {
                if (NetworkClient.active)
                {
                    NetworkClient.RegisterHandler<SceneOperationRequest>(Client_OnSceneOperationRequest);
                }
                else if (NetworkServer.active)
                {
                    NetworkServer.RegisterHandler<SceneOperationRequest>(Server_OnSceneOperationRequest);
                    NetworkServer.RegisterHandler<SceneOperationInfo>(Server_OnSceneOperationInfo);
                }
                Initialized = true;
            }
            else
            {
                Debug.LogWarning("Trying to init initialized scene manager.");
            }
        }

        private void Server_OnSceneOperationInfo(NetworkConnection conn, SceneOperationInfo msg)
        {
            // TODO manage separate scenes with clients in dict
            server_clientFinishedLoad = msg.loaded ? 2 : 1;
        }

        #region Server
        [Server]
        private void Server_OnSceneOperationRequest(NetworkConnection conn, SceneOperationRequest msg)
        {
            // Tell the specific client to also load the scene
            conn.Send(new SceneOperationRequest() { sceneName = msg.sceneName });

            var sceneLoadOp = SceneManager.LoadSceneAsync(msg.sceneName, new LoadSceneParameters(LoadSceneMode.Additive));
            sceneLoadOp.completed += Server_OnFinishedSceneLoading;
        }

        [Server]
        private void Server_OnFinishedSceneLoading(AsyncOperation op)
        {
            Debug.LogError("Not finished");
            // TODO server_clientFinishedLoad to enum
            if (server_clientFinishedLoad == 0)
            {
                StartCoroutine(WaitForClientToLoadScene());
            }
            else
            {
                // TODO Move player and unload scene if empty
            }
        }

        [Server]
        private IEnumerator WaitForClientToLoadScene()
        {
            // TODO 
            if (server_clientFinishedLoad == 0) yield return new WaitForSeconds(2);
            else
            {
                // TODO
            }
        }

        #endregion

            #region Client
        [Client]
        private void Client_OnSceneOperationRequest(SceneOperationRequest msg)
        {
            client_currentLoadingProgress = 0;
            AsyncOperation sceneLoadOp = SceneManager.LoadSceneAsync(msg.sceneName, new LoadSceneParameters(LoadSceneMode.Additive));
            StartCoroutine(HandleLoadSceneOp(sceneLoadOp, msg.sceneName));
        }

        [Client]
        private IEnumerator HandleLoadSceneOp(AsyncOperation asyncOperation, string sceneName)
        {
            // TODO do this in a coroutine?
            var startTime = Time.time;
            while (!asyncOperation.isDone)
            {
                client_currentLoadingProgress = asyncOperation.progress;
                if (Time.time - startTime > maxSceneLoadingTime)
                {
                    // Tell the server that the loading failed
                    NetworkClient.Send(new SceneOperationInfo() { sceneName = sceneName, loaded = false });
                    // TODO Disconnect client if loading fails?
                }
                // Wait for the next frame
                yield return null;
            }
            // Tell the server that the scene has been succesfully loaded
            NetworkClient.Send(new SceneOperationInfo() { sceneName = sceneName, loaded = true });
        }

        [Client]
        public void LoadScene(string sceneName)
        {
            if (Initialized)
            {
                NetworkClient.Send(new SceneOperationRequest() { sceneName = sceneName });
            }
            else
            {
                Debug.LogError("Trying to load scene, but the scene manager has not been initialized.");
            }
        }
        #endregion
    }
}
