// Created by Vojtech Bruza
using UnityEngine;

namespace UVRN
{
    public class UVRN_FailedLoginLobbyText : MonoBehaviour
    {
        private void Start()
        {
            GetComponent<TextMesh>().text = (UVRN_NetworkManager.singleton as UVRN_NetworkManager)?.PlayerManager?.failedAuthMessage;
        }
    }
}
