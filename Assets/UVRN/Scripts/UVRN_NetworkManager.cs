﻿// Created by Vojtech Bruza
using Mirror;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UVRN.Logging;
using UVRN.Player;
using UVRN.SceneManagement;
using UVRN.VoiceChat;

namespace UVRN
{
    public class UVRN_NetworkManager : NetworkManager
    {
        public enum ConnectionType
        {
            Client,
            Server
        };

        [Header("Connection Type")]
        [SerializeField]
        protected ConnectionType connectionType;


        public class UnityEventNetworkConnection : UnityEvent<NetworkConnection> { }
        [Serializable]
        public class UnityEventString : UnityEvent<string> { }
        [Serializable]
        public class UnityEventSceneChange : UnityEvent<string, SceneOperation, bool> { }
        public class UnityEventServerNetworkError : UnityEvent<NetworkConnection, int> { }
        public class UnityEventClientNetworkError : UnityEvent<int> { }

        public int ConnectionCount => NetworkServer.connections.Count;

        [Header("Custom Fields")]
        [SerializeField]
        protected UVRN_VoiceChatManager voiceChatManagerPrefab = null;

        protected UVRN_VoiceChatManager m_voiceChatManager = null;
        public UVRN_VoiceChatManager VoiceChatManager => m_voiceChatManager;

        /// <summary>
        /// Logger gameobject which should be disabled. Is automaticly enabled on the server only.
        /// </summary>
        [SerializeField]
        protected UVRN_Logger logger;

        [SerializeField]
        [Tooltip("Automatically connect client when the app is started in client mode")]
        protected bool autoConnectClient = false;

        [SerializeField]
        [Tooltip("Automatically load OfflineScene on client or server start")]
        protected bool autoLoadOfflineScene = false;

        /// <summary>
        /// Asign in the editor and make it a child object
        /// </summary>
        public UVRN_PlayerManager PlayerManager;

        public override void Awake()
        {
            base.Awake();

            if (voiceChatManagerPrefab == null)
            {
                Debug.LogError("Missing voice chat manager prefab");
            }
            else
            {
                m_voiceChatManager = Instantiate(voiceChatManagerPrefab, transform);
            }
        }

        public override void Start()
        {
#if UNITY_SERVER
            if (autoStartServerBuild)
            {
                StartServer();
            }
            return;
#endif
            // Server starts automatically
            if (connectionType == ConnectionType.Server)
            {
                StartServer();
            }
            // autoconnect client
            else if (connectionType == ConnectionType.Client)
            {
                if (autoConnectClient)
                {
                    StartClient();
                }
                else if (autoLoadOfflineScene)
                {
                    SceneManager.LoadScene(offlineScene);
                }
            }
        }

        #region Public properties

        public ushort Port
        {
            get => ((kcp2k.KcpTransport)transport).Port;
            set => ((kcp2k.KcpTransport)transport).Port = value;
        }

        #endregion

        #region Server Events

        [Header("Custom Server Events")]

        public UnityEvent Server_OnStart = new UnityEvent();
        public UnityEvent Server_OnStop = new UnityEvent();

        public UnityEventNetworkConnection Server_OnClientConnecting = new UnityEventNetworkConnection();
        public UnityEventNetworkConnection Server_OnClientConnected = new UnityEventNetworkConnection();

        /// <summary>
        /// Called before destroying player for connection.
        /// </summary>
        public UnityEventNetworkConnection Server_OnClientDisconnecting = new UnityEventNetworkConnection();

        /// <summary>
        /// Called after destroying player for connection.
        /// </summary>
        public UnityEventNetworkConnection Server_OnClientDisconnected = new UnityEventNetworkConnection();

        public UnityEventNetworkConnection Server_OnAddPlayer = new UnityEventNetworkConnection();

        public UnityEventNetworkConnection Server_OnClientReady = new UnityEventNetworkConnection();

        /// <summary>
        /// Invoked from ServerChangeScene immediately before SceneManager.LoadSceneAsync is executed
        /// </summary>
        public UnityEventString Server_OnSceneChange = new UnityEventString();

        public UnityEventString Server_OnSceneChanged = new UnityEventString();

        public UnityEventServerNetworkError Server_OnError = new UnityEventServerNetworkError();

        #endregion

        #region Client Events

        [Header("Custom Client Events")]

        public UnityEvent Client_OnStart = new UnityEvent();
        public UnityEvent Client_OnStop = new UnityEvent();
        /// <summary>
        /// Called before trying to connect to the server.
        /// </summary>
        public UnityEvent Client_OnConnectToServer = new UnityEvent();
        /// <summary>
        /// Called before disconnecting from the server.
        /// Also when failed to connect to a server.
        /// </summary>
        public UnityEvent Client_OnDisconnectFromServer = new UnityEvent();

        /// <summary>
        /// Invoked from ClientChangeScene immediately before SceneManager.LoadSceneAsync is executed.
        /// </summary>
        public UnityEventSceneChange Client_OnChangeScene = new UnityEventSceneChange();

        /// <summary>
        /// Called only when the scene change was initiated by the server.
        /// </summary>
        public UnityEvent Client_OnSceneChanged = new UnityEvent();

        public UnityEvent Client_OnNotReady = new UnityEvent();

        public UnityEventClientNetworkError Client_OnError = new UnityEventClientNetworkError();

        public UnityEvent Client_OnIdentityReceived = new UnityEvent();

        #endregion

        #region Other Public Methods

        public void SetNetworkAddress(string address)
        {
            networkAddress = address;
        }

        public override void ServerChangeScene(string newSceneName)
        {
            base.ServerChangeScene(newSceneName);
        }

        #endregion

        #region Server Overrides

        public override void OnStartServer()
        {
            logger?.gameObject?.SetActive(true);
            Server_OnStart?.Invoke();
        }

        public override void OnStopServer() => Server_OnStop?.Invoke();

        public override void OnServerSceneChanged(string sceneName)
        {
            Debug.Log("Scene changed to " + sceneName);
            Server_OnSceneChanged?.Invoke(sceneName);
        }

        public override void OnServerAddPlayer(NetworkConnection conn)
        {
            Debug.Log("Adding player for connection with ID: " + conn.connectionId);
            // TODO remove this logic from the manager - add the player somewhere else?
            Transform startPos = GetStartPosition();
            GameObject player = startPos != null
                ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
                : Instantiate(playerPrefab);

            NetworkServer.AddPlayerForConnection(conn, player);

            Server_OnAddPlayer?.Invoke(conn);
            Debug.Log("Player added.");
        }

        public override void OnServerConnect(NetworkConnection conn)
        {
            Debug.Log("Client connected " + conn.connectionId);
            Server_OnClientConnecting?.Invoke(conn);
            Debug.Log("Total connections " + ConnectionCount);
            if (ConnectionCount > maxConnections)
            {
                conn.Disconnect();
                Debug.Log("Too many client connections. Could not connect new client.");
                // TODO send message to client
                return;
            }

            Server_OnClientConnected?.Invoke(conn);
        }

        public override void OnServerDisconnect(NetworkConnection conn)
        {
            Debug.Log("Client disconnected " + conn.connectionId);
            Server_OnClientDisconnecting?.Invoke(conn);
            base.OnServerDisconnect(conn);
            Server_OnClientDisconnected?.Invoke(conn);
        }

        public override void OnServerChangeScene(string newSceneName)
        {
            Debug.Log("Changing scene to " + newSceneName);
            Server_OnSceneChange?.Invoke(newSceneName);
        }

        public override void OnServerReady(NetworkConnection conn)
        {
            Debug.Log("Client ready. ID: " + conn.connectionId);
            base.OnServerReady(conn);
            Server_OnClientReady?.Invoke(conn);
        }

        [Obsolete]
        public override void OnServerError(NetworkConnection conn, int errorCode)
        {
            Debug.Log("Server error: " + errorCode + ", Connection: " + conn.connectionId);
            Server_OnError?.Invoke(conn, errorCode);
        }

        #endregion

        #region Client Overrides

        public override void OnStartClient()
        {
            Debug.Log("Connecting to: " + networkAddress);
            Client_OnStart?.Invoke();
        }

        public override void OnStopClient()
        {
            Debug.Log("Client stopped.");
            Client_OnStop?.Invoke();
        }

        // the parameter is redundant and gets removed in newer versions of Mirror
        public override void OnClientConnect(NetworkConnection conn)
        {
            base.OnClientConnect(conn);
            Debug.Log("Connected to server.");
            Client_OnConnectToServer?.Invoke();

            // TODO wait for server response somewhere and call OnConnected client event
        }

        // the parameter is redundant and gets removed in newer versions of Mirror
        public override void OnClientDisconnect(NetworkConnection conn)
        {
            Debug.Log("Disconnected.");
            Client_OnDisconnectFromServer?.Invoke();
            base.OnClientDisconnect(conn);
        }

        public override void OnClientChangeScene(string newSceneName, SceneOperation sceneOperation, bool customHandling)
        {
            Client_OnChangeScene?.Invoke(newSceneName, sceneOperation, customHandling);
        }
        
        // the parameter is redundant and gets removed in newer versions of Mirror
        public override void OnClientSceneChanged(NetworkConnection conn)
        {
            base.OnClientSceneChanged(conn);
            Client_OnSceneChanged?.Invoke();
        }

        // the parameter is redundant and gets removed in newer versions of Mirror
        public override void OnClientNotReady(NetworkConnection conn)
        {
            Client_OnNotReady?.Invoke();
        }

        [Obsolete]
        // the connection parameter is redundant and gets removed in newer versions of Mirror
        public override void OnClientError(NetworkConnection conn, int errorCode)
        {
            Debug.LogError("Client error: " + errorCode);
            Client_OnError?.Invoke(errorCode);
        }

        #endregion

        [Client]
        public void Disconnect()
        {
            if (NetworkClient.isConnected)
            {
                Debug.Log("Disconnecting...");
                StopClient();
            }
        }
    }
}
