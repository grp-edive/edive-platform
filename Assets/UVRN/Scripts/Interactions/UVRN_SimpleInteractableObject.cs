// Created by Vojtech Bruza
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace UVRN.Interactions {
    [RequireComponent(typeof(XRSimpleInteractable))]
    public class UVRN_SimpleInteractableObject : NetworkBehaviour
    {
        private XRSimpleInteractable simpleInteractable;

        [Tooltip("Called on the client after validated on the server")]
        public UnityEvent Client_SelectExitedRPC = new UnityEvent();
        [Tooltip("Called on the server when a client interacts with simple interactable")]
        public UnityEvent Server_SelectExited = new UnityEvent(); // TODO remove, but be careful about serialized events in the editor
        [Tooltip("Called on the server when a client interacts with simple interactable (contains the sender)")]
        public UnityEventNetworkConnection Server_SelectExitedSender = new UnityEventNetworkConnection();

        #region XRIT Callbacks
        protected virtual void XRIT_OnSelectExited(SelectExitEventArgs args)
        {
            Cmd_SelectExited();
        }
        #endregion

        #region Unity Callbacks
        protected void Awake()
        {
            simpleInteractable = GetComponent<XRSimpleInteractable>();
        }

        #endregion

        #region Mirror Overrides
        public override void OnStartClient()
        {
            simpleInteractable.selectExited.AddListener(XRIT_OnSelectExited);
        }

        public override void OnStopClient()
        {
            simpleInteractable.selectExited.RemoveListener(XRIT_OnSelectExited);
        }

        public override void OnStartServer()
        {
        }

        public override void OnStopServer()
        {
        }
        #endregion

        #region Commands
        [Command(requiresAuthority = false)]
        private void Cmd_SelectExited(NetworkConnectionToClient sender = null)
        {
            Server_SelectExited.Invoke();
            Server_SelectExitedSender.Invoke(sender);
            Rpc_SelectExited();
        }
        #endregion

        #region RPCs
        [ClientRpc]
        private void Rpc_SelectExited()
        {
            Client_SelectExitedRPC.Invoke();
        }
        #endregion
    }
}
