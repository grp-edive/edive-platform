// Created by Vojtech Bruza
using UnityEngine;

public class UVRN_VisualToggle : MonoBehaviour
{
    [SerializeField]
    private GameObject visuals;

    public void Toggle(bool active)
    {
        visuals.SetActive(active);
    }
}
