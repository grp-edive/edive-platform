using UnityEngine;
using Mirror;
using UVRN.Logging;

namespace UVRN.Interactions.Logging
{
    [RequireComponent(typeof(UVRN_GrabInteractableObject))]
    class UVRN_GrabInteractibleObjectLogger : UVRN_GenericLogger
    {
        private UVRN_GrabInteractableObject grabInteractable;

        public override void Init()
        {
            Init(null, "Grab Interactable", true);

            grabInteractable = GetComponent<UVRN_GrabInteractableObject>();

            grabInteractable.Server_ClientGrabbedObject.AddListener(LogObjectGrabbed);
            grabInteractable.Server_ClientDroppedObject.AddListener(LogObjectDropped);
        }

        private void LogObjectGrabbed(NetworkConnection grabbingPlayerConnection)
        {
            LogCustomMessage("object grabbed by", GetNameFromConnection(grabbingPlayerConnection));
        }

        private void LogObjectDropped(NetworkConnection droppingPlayerConnection)
        {
            LogCustomMessage("object dropped by", GetNameFromConnection(droppingPlayerConnection));
        }
    }
}