﻿// Created by Vojtech Bruza
using Mirror;
using MUVRE;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;
using UVRN.Logging;
using UVRN.Player;

namespace UVRN
{
    /// <summary>
    /// Give the authority back to the server when the player drops
    /// </summary>
    [RequireComponent(typeof(XRGrabInteractable), typeof(NetworkIdentity), typeof(NetworkTransform))]
    public class UVRN_GrabInteractableObjectOld : NetworkBehaviour
    {
        #region Net Manager
        private UVRN_NetworkManager m_netManager;
        private UVRN_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }
        #endregion

        public UnityEventUint Server_SomeoneGrabbed = new UnityEventUint();
        public UnityEventUint Client_Other_Grabbed = new UnityEventUint();
        public UnityEvent Client_This_Grabbed = new UnityEvent();

        public UnityEvent Server_SomeoneDropped = new UnityEvent();
        public UnityEvent Client_Someone_Dropped = new UnityEvent();
        
        // TODO move activate to simple interactable and use syncvar to sync state
        public UnityEventUint Server_SomeoneActivated = new UnityEventUint();
        public UnityEvent Client_SomeoneActivated = new UnityEvent();


        private XRGrabInteractable grabInteractable;
        private NetworkTransform networkTransform;
        private Rigidbody rb;

        /// <summary>
        /// null if server
        /// </summary>
        private NetworkConnectionToClient authorityOwner;

        private bool server_PhysicsEnabled;

        /// <summary>
        /// Should the gravity be turned back on on the server after the client drops this object?
        /// </summary>
        private bool server_rbUsedGravity;

        /// <summary>
        /// Should the isKinematic be turned back on on the server after the client drops this object?
        /// </summary>
        private bool server_rbWasKinematic;

        [SyncVar(hook = nameof(SyncGrabbedByPlayer))]
        private uint grabbedByPlayer = 0;

        #region XRIT Callbacks && Client Methods
        [Client]
        private void XRIT_OnObjectGrabbed(SelectEnterEventArgs args)
        {
            // already grabbing e.g. with a different interactor
            if (grabbedByPlayer == netManager.PlayerManager.LocalPlayerID) return;

            if (grabbedByPlayer != 0)
            {
                Debug.Log("Cannot grab, someone already is an owner.");
            }
            else Cmd_Grab();
        }

        [Client]
        private void XRIT_OnObjectReleased(SelectExitEventArgs args)
        {
            if (args.interactable.hoveringInteractors.Count > 1) StartCoroutine(CheckIfGrabbedToDrop());
            else Drop();
        }

        /// <summary>
        /// To be able to check if the object was not just grabbed by another interactor
        /// </summary>
        /// <returns></returns>
        [Client]
        private IEnumerator CheckIfGrabbedToDrop()
        {
            yield return new WaitForEndOfFrame();
            // if not holding by another interactor
            if (!grabInteractable.isSelected)
            {
                Drop();
            }
        }

        [Client]
        private void Drop()
        {
            if (grabbedByPlayer == netManager.PlayerManager.LocalPlayerID) Cmd_Drop();
            else Debug.Log("You cannot drop object that you are not holding.");
        }

        [Client]
        private void XRIT_OnActivate(ActivateEventArgs args)
        {
            Cmd_Activate();
        }
        #endregion

        #region SyncVar Hooks

        /// <summary>
        /// Disable grabbing for all other player
        /// </summary>
        /// <param name="_old"></param>
        /// <param name="_new"></param>
        private void SyncGrabbedByPlayer(uint _old, uint _new)
        {
            grabbedByPlayer = _new; // for the server to be able to set this via the sync function
            if (_new == 0)
            {
                // "grabbed" by server
                // reactivate interactions
                grabInteractable.enabled = true;
            }
            else if (netManager.PlayerManager.LocalPlayerID == _new)
            {
                networkTransform.clientAuthority = true; // sets for this client

                Client_This_Grabbed.Invoke();
            }
            else
            {
                // deactivate interactions for all other clients
                grabInteractable.enabled = false;

                Client_Other_Grabbed.Invoke(_new);
            }
        }

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            if (!grabInteractable) grabInteractable = GetComponent<XRGrabInteractable>();
            networkTransform = GetComponent<NetworkTransform>();

            //networkManager.OnServerStarted
            //networkManager.OnServerStopped
            if (!rb) rb = GetComponent<Rigidbody>();

            server_rbWasKinematic = rb.isKinematic;
            server_rbUsedGravity = rb.useGravity;

            server_PhysicsEnabled = true;

            if (!rb.isKinematic)
            {
                // must be set to instantaneous, otherwise the rigidbody interferes with the movement
                if (grabInteractable.movementType != XRBaseInteractable.MovementType.Instantaneous)
                {
                    grabInteractable.movementType = XRBaseInteractable.MovementType.Instantaneous;
                    Debug.LogError("Movement type must be se to " + XRBaseInteractable.MovementType.Instantaneous);
                }
                Debug.LogError("Cannot work with non-kinematic rigidbodies yet");
            }
            if (grabInteractable.movementType != XRBaseInteractable.MovementType.Kinematic)
            {
                Debug.LogError("Only kinematic now suported?");
            }
            if (GetComponent<NetworkTransform>().syncScale)
            {
                Debug.LogWarning("Sync scale over network is unnecessarily performance heavy");
            }
            if (rb.useGravity)
            {
                rb.useGravity = false;
                Debug.LogWarning("Turn of the gravity? Not solved yet and may cause trouble");
            }
        }

        private void Reset()
        {
            if (!grabInteractable) grabInteractable = GetComponent<XRGrabInteractableExtended>();
            //if (grabInteractable.movementType != XRBaseInteractable.MovementType.Instantaneous)
            //{
            //    grabInteractable.movementType = XRBaseInteractable.MovementType.Instantaneous;
            //    Debug.Log("Setting interactable movement type to " + XRBaseInteractable.MovementType.Instantaneous);
            //}
            if (!rb) rb = GetComponent<Rigidbody>();
            if (!rb.isKinematic)
            {
                rb.isKinematic = true;
            }
            GetComponent<NetworkTransform>().syncScale = false;
        }

        #endregion

        #region Commands

        [Command(requiresAuthority = false)]
        private void Cmd_Activate(NetworkConnectionToClient sender = null)
        {
            if (authorityOwner == null)
            {
                Debug.Log("Cannot activate, there is no owner.");
                return;
            }
            var owner = sender?.identity.GetComponent<UVRN_Player>();
            if (owner != null && sender == authorityOwner)
            {
                // Log on server
                UVRN_Logger.Instance.LogEntry(owner.ID.ToString(), $"Object '{gameObject.name}' activated");

                Server_SomeoneActivated.Invoke(owner.ID);

                // Notify all clients that someone activated
                Rpc_Activate();
            }
        }

        /// <summary>
        /// Assign authority to the sender and disable physics on the server.
        /// </summary>
        /// <param name="sender"></param>
        [Command(requiresAuthority = false)]
        private void Cmd_Grab(NetworkConnectionToClient sender = null)
        {
            if (authorityOwner == sender)
            {
                // just switching interactors (same user grabs with a different hand)
                return;
            }
            if (authorityOwner != null)
            {
                Debug.Log("Cannot grab, someone else is already an owner.");
                return;
            }
            // Notify all clients that someone grabbed
            var newOwnerPlayer = sender?.identity.GetComponent<UVRN_Player>();
            if (newOwnerPlayer != null)
            {
                SyncGrabbedByPlayer(grabbedByPlayer, newOwnerPlayer.ID);

                // assign authority to the client (both this object and its network transform)
                authorityOwner = sender;
                netIdentity.AssignClientAuthority(authorityOwner);
                networkTransform.netIdentity.AssignClientAuthority(authorityOwner);
                networkTransform.clientAuthority = true; // set only on server (not synced)

                // Disable physics on the server
                Server_DisablePhysics();

                // Log on server
                UVRN_Logger.Instance.LogEntry(newOwnerPlayer.ID.ToString(), $"Object '{gameObject.name}' grabbed");

                Server_SomeoneGrabbed.Invoke(newOwnerPlayer.ID);
            }
        }

        // TODO apply physics from the client (throw on detach) or keep the authority on the client until he leaves
        [Command]
        private void Cmd_Drop(NetworkConnectionToClient sender = null)
        {
            if (sender != null && sender == authorityOwner)
            {
                Server_Drop();
            }
        }

        [Server]
        private void Server_Drop()
        {
            // Log on server (the previous owner can be null at start)
            var prevOwner = authorityOwner?.identity.GetComponent<UVRN_Player>();
            if (prevOwner) UVRN_Logger.Instance.LogEntry(prevOwner.ID.ToString(), $"Object '{gameObject.name}' dropped");

            // player ID is always greater than 0, so 0 means "server" in this case
            SyncGrabbedByPlayer(grabbedByPlayer, 0);

            // give authority back to server
            authorityOwner = null;
            netIdentity.RemoveClientAuthority();
            networkTransform.netIdentity.RemoveClientAuthority();
            networkTransform.clientAuthority = false; // set only on server (not synced)

            // EnablePhysics
            Server_EnablePhysics();

            // Setup grabbed object on all clients
            Rpc_Drop();

            Server_SomeoneDropped.Invoke();
        }

        #endregion

        #region RPCs

        /// <summary>
        /// Notify all clients that object is dropped
        /// </summary>
        [ClientRpc]
        private void Rpc_Drop()
        {
            networkTransform.clientAuthority = false; // set only on client (not synced)

            Client_Someone_Dropped.Invoke();
        }

        /// <summary>
        /// Notify all clients that object is activated
        /// </summary>
        [ClientRpc]
        private void Rpc_Activate()
        {
            Client_SomeoneActivated.Invoke();
        }
        
        #endregion

        #region Mirror Overrides

        public override void OnStartClient()
        {
            grabInteractable.selectEntered.AddListener(XRIT_OnObjectGrabbed);
            grabInteractable.selectExited.AddListener(XRIT_OnObjectReleased);
            grabInteractable.activated.AddListener(XRIT_OnActivate);

            // disable interactions with object if someone is holding it
            if (grabbedByPlayer != 0)
            {
                grabInteractable.enabled = false;
            }
        }

        public override void OnStopClient()
        {
            grabInteractable.selectEntered.RemoveListener(XRIT_OnObjectGrabbed);
            grabInteractable.selectExited.RemoveListener(XRIT_OnObjectReleased);
            grabInteractable.activated.RemoveListener(XRIT_OnActivate);
        }

        public override void OnStartServer()
        {
            netManager.Server_OnClientDisconnecting.AddListener(Server_OnClientDisconnecting);
        }

        public override void OnStopServer()
        {
            netManager.Server_OnClientDisconnecting.AddListener(Server_OnClientDisconnecting);
        }

        #endregion

        #region Server Methods
        /// <summary>
        /// Disable physics on the server
        [Server]
        private void Server_DisablePhysics()
        {
            if (server_PhysicsEnabled)
            {
                // Stop physics on the server
                // save previous values
                server_rbUsedGravity = rb.useGravity;
                server_rbWasKinematic = rb.isKinematic;
                // disable rb to not interfere with grabbing movement
                rb.isKinematic = true;
                rb.useGravity = false;

                server_PhysicsEnabled = false;
            }
        }

        /// </summary>
        /// <summary>
        /// Enable physics on the server
        /// </summary>
        [Server]
        public void Server_EnablePhysics()
        {
            if (!server_PhysicsEnabled)
            {
                // Start physics back on the server
                // restore saved values
                rb.useGravity = server_rbUsedGravity;
                rb.isKinematic = server_rbWasKinematic;

                server_PhysicsEnabled = true;
            }
        }

        [Server]
        private void Server_OnClientDisconnecting(NetworkConnection conn)
        {
            var leavingPlayer = conn.identity.GetComponent<UVRN_Player>();
            if (leavingPlayer && leavingPlayer.ID == grabbedByPlayer) Server_Drop();
        }

        #endregion
    }
}
