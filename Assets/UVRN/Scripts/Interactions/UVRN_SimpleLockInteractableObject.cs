﻿// Created by Vojtech Bruza
using Mirror;
using MUVRE;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;
using UVRN.Logging;
using UVRN.Player;

namespace UVRN.Interactions
{
    /// <summary>
    /// Idea is to ask for authority first, then immediately grab it. The again when dropped give authority back to server.
    /// </summary>
    [RequireComponent(typeof(XRBaseInteractable), typeof(NetworkIdentity), typeof(NetworkTransform))]
    public class UVRN_SimpleLockInteractableObject : NetworkBehaviour
    {
        #region Net Manager
        private UVRN_NetworkManager m_netManager;
        private UVRN_NetworkManager netManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }
        #endregion


        #region Events
        // used for logging change in the object being grabbed (string playerID)
        public UnityEvent<NetworkConnection> Server_ClientGrabbedObject = new UnityEvent<NetworkConnection>();
        public UnityEvent<NetworkConnection> Server_ClientDroppedObject = new UnityEvent<NetworkConnection>();

        public UnityEvent Client_Other_Grabbed = new UnityEvent();
        public UnityEvent Client_This_Grabbed = new UnityEvent();

        public UnityEvent Client_Someone_Dropped = new UnityEvent();
        public UnityEvent Client_This_Dropped = new UnityEvent();

        // TODO move activate to simple interactable and use syncvar to sync state
        public UnityEvent Server_SomeoneActivated = new UnityEvent();
        public UnityEvent Client_SomeoneActivated = new UnityEvent();

        #endregion


        #region Fields & Properties

        protected XRBaseInteractable interactableObject;
        protected NetworkTransform networkTransform;
        protected Rigidbody rb;

        [Tooltip("Time in seconds to wait before telling the server that this client dropped the object (to prevent spamming).")]
        public float waitToDropTime = .5f;
        private IEnumerator delayedDrop;


        [Tooltip("Time in seconds to wait before force drop if the there is no movement with the object detected.")]
        public float inactiveTimeout = 8;
        private float lastMoveTime;
        //private float moveDetectionTershold = 0.001f;
        // in degrees
        //private float rotationDetectionTershold = 0.01f;
        protected Vector3 prevFramePosition;
        protected Quaternion prevFrameRotation;


        /// <summary>
        /// This is needed because when a new client is connecting, there is no other way to tell that someone is holding the object.
        /// </summary>
        [SyncVar(hook = nameof(SyncGrabbed))]
        private bool grabbed;

        #endregion

        private void SyncGrabbed(bool _old, bool _new)
        {
            grabbed = _new;
            if (grabbed)
            {
                networkTransform.clientAuthority = true;
                // grabbed by the local player
                if (hasAuthority)
                {
                    Debug.Log("Grabbed and has authority");
                    Client_This_Grabbed.Invoke();
                }
                else
                {
                    interactableObject.enabled = false;
                    Client_Other_Grabbed.Invoke();
                }
            }
            else
            {
                // dropped
                interactableObject.enabled = true;
                networkTransform.clientAuthority = false;
            }
        }

        #region XRIT Callbacks && Client Methods

        [Client]
        private void XRIT_OnSelectEntered(SelectEnterEventArgs args)
        {
            if (hasAuthority)
            {
                StopCoroutine(delayedDrop); // stop the drop if the object has been grabbed by different interactor
            }
            else
            {
                Cmd_AskForAuthorityToGrab();
            }
        }

        [Client]
        private void XRIT_OnActivated(ActivateEventArgs args)
        {
            if (hasAuthority) Cmd_Activate();
        }

        [Client]
        private void XRIT_OnSelectExited(SelectExitEventArgs args)
        {
            if (hasAuthority)
            {
                delayedDrop = DelayedDrop(waitToDropTime);
                StartCoroutine(delayedDrop);
            }
        }

        /// <summary>
        /// To be able to check if the object was not just grabbed by another interactor
        /// </summary>
        /// <returns></returns>
        [Client]
        private IEnumerator DelayedDrop(float waitTime)
        {
            yield return new WaitForSecondsRealtime(waitTime);
            Cmd_Drop();
        }

        #endregion


        #region Unity Callbacks

        protected virtual void Awake()
        {
            if (!interactableObject) interactableObject = GetComponent<XRGrabInteractable>();
            networkTransform = GetComponent<NetworkTransform>();

            if (!rb) rb = GetComponent<Rigidbody>();

            if (GetComponent<NetworkTransform>().syncScale)
            {
                Debug.LogWarning("Sync scale over network is unnecessarily performance heavy");
            }
            if (rb.useGravity)
            {
                rb.useGravity = false;
                Debug.LogWarning("Turn of the gravity? Not solved yet and may cause trouble");
            }

            if (networkTransform.clientAuthority)
            {
                Debug.LogWarning("Setting clientAuthority to false. It gets automatically assigned when someone grabs the object.");
                networkTransform.clientAuthority = false;
            }
            if (networkTransform.netIdentity != netIdentity)
            {
                Debug.LogError("Network transform has a different network identity that this component.");
            }
        }

        protected virtual void Reset()
        {
            if (!interactableObject) interactableObject = GetComponent<XRGrabInteractableExtended>();
            if (!rb) rb = GetComponent<Rigidbody>();
            if (!rb.isKinematic)
            {
                rb.isKinematic = true;
            }
            GetComponent<NetworkTransform>().syncScale = false;
        }

        #endregion


        #region Commands

        /// <summary>
        /// Add authority and grab
        /// </summary>
        /// <param name="sender">Auto-assigned client by Mirror</param>
        [Command(requiresAuthority = false)]
        private void Cmd_AskForAuthorityToGrab(NetworkConnectionToClient sender = null)
        {
            // if owned by the server
            if (netIdentity.connectionToClient == null)
            {
                var newOwnerPlayer = sender?.identity?.GetComponent<UVRN_Player>();
                if (newOwnerPlayer != null)
                {
                    // assign authority
                    netIdentity.AssignClientAuthority(sender);
                    // it should be the same net identity, so we do not have to set it twice
                    //networkTransform.netIdentity.AssignClientAuthority(sender);

                    // grab
                    SyncGrabbed(grabbed, true);

                    // Log on server
                    Server_ClientGrabbedObject.Invoke(sender);
                }
                else
                {
                    Debug.LogError("Missing player component");
                }
            }
            else
            {
                Debug.Log("A client asked for authority over an object that has been already grabbed by a different client.");
            }
        }

        [Command]
        private void Cmd_Activate(NetworkConnectionToClient sender = null)
        {
            var owner = sender?.identity.GetComponent<UVRN_Player>();
            if (owner != null)
            {
                // Log on server
                UVRN_Logger.Instance.LogEntry(owner.ID.ToString(), $"Object '{gameObject.name}' activated");

                Server_SomeoneActivated.Invoke();

                // Notify all clients that someone activated
                Rpc_Activate();
            }
        }

        // TODO apply physics from the client (throw on detach) or keep the authority on the client until he leaves
        [Command]
        private void Cmd_Drop()
        {
            Server_Drop();
        }

        #endregion


        #region RPCs

        /// <summary>
        /// Notify all clients that object is dropped
        /// </summary>
        [ClientRpc]
        private void Rpc_Drop()
        {
            interactableObject.enabled = true;
            Client_Someone_Dropped.Invoke();
        }

        /// <summary>
        /// Notify all clients that object is activated
        /// </summary>
        [ClientRpc]
        private void Rpc_Activate()
        {
            Client_SomeoneActivated.Invoke();
        }

        #endregion


        #region Mirror Overrides

        // client can grab
        public override void OnStartAuthority()
        {
            Debug.Log("This client grabbed: " + gameObject.name);
            Client_This_Grabbed.Invoke();
        }

        // client dropped
        public override void OnStopAuthority()
        {
            Debug.Log("This client dropped: " + gameObject.name);
            Client_This_Dropped.Invoke();
        }

        public override void OnStartClient()
        {
            interactableObject.selectEntered.AddListener(XRIT_OnSelectEntered);
            interactableObject.selectExited.AddListener(XRIT_OnSelectExited);
            interactableObject.activated.AddListener(XRIT_OnActivated);

            // disable interactions with object if someone is holding it
            if (grabbed)
            {
                interactableObject.enabled = false;
            }
        }

        public override void OnStopClient()
        {
            interactableObject.selectEntered.RemoveListener(XRIT_OnSelectEntered);
            interactableObject.selectExited.RemoveListener(XRIT_OnSelectExited);
            interactableObject.activated.RemoveListener(XRIT_OnActivated);
        }

        public override void OnStartServer()
        {
            netManager.Server_OnClientDisconnecting.AddListener(Server_OnClientDisconnecting);
        }

        public override void OnStopServer()
        {
            netManager.Server_OnClientDisconnecting.AddListener(Server_OnClientDisconnecting);
        }

        #endregion


        #region Server Methods

        // TODO server physics methods - according to the old class (should somehow work)

        [Server]
        private void Server_OnClientDisconnecting(NetworkConnection leavingClient)
        {
            // if the object is held by the client that is leaving, drop it first
            if (netIdentity.connectionToClient == leavingClient) Server_Drop();
        }

        [Server]
        private void Server_Drop()
        {
            if (grabbed)
            {
                // Store prev owner before removing it (the previous owner can be null at start)
                var prevOwnerConnection = netIdentity.connectionToClient;
                var prevOwner = prevOwnerConnection?.identity?.GetComponent<UVRN_Player>();

                // give authority back to server
                netIdentity.RemoveClientAuthority();
                //networkTransform.netIdentity.RemoveClientAuthority();

                SyncGrabbed(grabbed, false);

                // Setup grabbed object on all clients
                Rpc_Drop();

                // event
                Server_ClientDroppedObject.Invoke(prevOwnerConnection);
                //Debug.Log("Client with id " + prevOwner?.ID + " dropped " + gameObject.name);
            }
            else
            {
                Debug.Log("Cannot drop object which is not grabbed");
            }
        }

        #endregion
    }
}
