﻿// Created by Vojtech Bruza
using Mirror;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace UVRN.Interactions
{
    [RequireComponent(typeof(XRGrabInteractable), typeof(NetworkIdentity), typeof(NetworkTransform))]
    public class UVRN_GrabInteractableObject : UVRN_SimpleLockInteractableObject
    {
        protected override void Awake()
        {
            base.Awake();

            var grabInteractable = (XRGrabInteractable)interactableObject;
            if (grabInteractable == null)
            {
                Debug.LogError("Must be grab interactable");
            }

            if (!rb.isKinematic)
            {
                // must be set to instantaneous, otherwise the rigidbody interferes with the movement
                if (grabInteractable.movementType != XRBaseInteractable.MovementType.Instantaneous)
                {
                    grabInteractable.movementType = XRBaseInteractable.MovementType.Instantaneous;
                    Debug.LogError("Movement type must be se to " + XRBaseInteractable.MovementType.Instantaneous);
                }
                Debug.LogError("Cannot work with non-kinematic rigidbodies yet");
            }
            if (grabInteractable.movementType != XRBaseInteractable.MovementType.Kinematic)
            {
                Debug.LogError("Only kinematic now suported?");
            }
        }
    }
}
