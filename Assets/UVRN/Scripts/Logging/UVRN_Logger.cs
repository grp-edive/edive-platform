﻿// Created by Vladimir Zbanek, Vojtech Bruza
using UnityEngine;
using System;
using System.IO;
using UVRN.Helpers;
using System.Globalization;
using Mirror;
using UVRN.Player;

namespace UVRN.Logging
{
    public class UVRN_Logger : Singleton<UVRN_Logger>
    {
        public bool Initialized { get; private set; }
        public bool LogToConsole { get; set; } = false;
        public char ColumnSeparator { get; set; }

        private StreamWriter logFile;

        #region methods
        /// <summary>
        /// Initialize all the values and setup the logger.
        /// </summary>
        /// <param name="folder">Location for the log file.</param>
        /// <param name="filename">Name for the log file.</param>
        /// <param name="columnSeparator">A character to separate the values.</param>
        /// <param name="logToConsole">Write all entries to  debug console.</param>
        public void Init(string folder, string filename, char columnSeparator = ';', bool logToConsole = false)
        {
            this.ColumnSeparator = columnSeparator;
            this.LogToConsole = logToConsole;

            string path = folder + "/" + String.Format("{0:yyyy-MM-dd-HH-mm}", DateTime.Now);

            Debug.Log("Logger initialization...");

            //create directory if it don't exists already
            Directory.CreateDirectory(path);

            string logPath = path + "/" + filename;
            Debug.Log("Log will be written to: '" + logPath);

            bool addCreatedEntry = false;

            //check if log already exists
            if (!File.Exists(logPath))
            {
                addCreatedEntry = true;
            }

            //create new stream writer - will create new or append to existing file
            try
            {
                logFile = new StreamWriter(logPath, true);
            }
            catch (IOException e)
            {
                Debug.LogWarning("Log file creation failed!");
                Debug.LogWarning(e);
            }

            if (addCreatedEntry)
            {
                LogEntry("Server", "Log file created");
            }

            Initialized = true;
            LogEntry("Server", "New session started");
        }

        #region transform logging methods
        public void LogPosition(UVRN_GenericLogger logger, Vector3 value)
        {
            string textToLog = $"{logger.ObjectType}{ColumnSeparator}position{ColumnSeparator}{value}";
            LogEntry(logger.ObjectID, textToLog, false);
        }

        public void LogRotation(UVRN_GenericLogger logger, Quaternion value)
        {
            string textToLog = $"{logger.ObjectType}{ColumnSeparator}rotation{ColumnSeparator}{value.eulerAngles}";
            LogEntry(logger.ObjectID, textToLog, false);
        }

        public void LogLocalScale(UVRN_GenericLogger logger, Vector3 value)
        {
            string textToLog = $"{logger.ObjectType}{ColumnSeparator}local scale{ColumnSeparator}{value}";
            LogEntry(logger.ObjectID, textToLog, false);
        }
        #endregion

        /// <summary>
        /// Log a custom message. Message parts will be separated into columns.
        /// </summary>
        /// <param name="logger">Instance of the calling logger.</param>
        /// <param name="message">Messages to log, parts will be separated into columns.</param>
        public void LogCustomMessage(UVRN_GenericLogger logger, params string[] message)
        {
            string textToLog = $"{logger.ObjectType}";
            foreach (string part in message)
            {
                textToLog += $"{ColumnSeparator}{part}";
            }
            LogEntry(logger.ObjectID, textToLog);
        }

        [Obsolete("Please use LogPosition/Rotation/LocalScale(...) or LogCustomMessage(...) instead")]
        public void LogEntry(string userID, Transform transform, string entry = "", bool logToConsole = false)
        {
            var x = transform.position.x.ToString("G", CultureInfo.InvariantCulture);
            var y = transform.position.y.ToString("G", CultureInfo.InvariantCulture);
            var z = transform.position.z.ToString("G", CultureInfo.InvariantCulture);
            var rotx = transform.rotation.eulerAngles.x.ToString("G", CultureInfo.InvariantCulture);
            var roty = transform.rotation.eulerAngles.y.ToString("G", CultureInfo.InvariantCulture);
            var rotz = transform.rotation.eulerAngles.z.ToString("G", CultureInfo.InvariantCulture);
            var text = $"position ({x}, {y}, {z}){ColumnSeparator}euler angles ({rotx}, {roty}, {rotz})";
            if (!string.IsNullOrWhiteSpace(entry)) text += $"{ColumnSeparator}{entry}";
            LogEntry(userID, text, logToConsole);
        }

        // TODO: don't know what to do with this one, should all logs go through a logger?
        public void LogEntry(NetworkConnectionToClient sender, string entry, bool logToConsole = true)
        {
            var player = sender?.identity.GetComponent<UVRN_Player>();
            if (player != null)
            {
                LogEntry(player.ID.ToString(), entry, logToConsole);
            }
            else
            {
                Debug.LogError("There was no player component to log.");
            }
        }

        // writes to file
        // TODO: bypasses standartization, should be marked as private
        public void LogEntry(string objectID, string entry, bool logToConsole = true)
        {
            if (logFile != null)
            {
                // not using millis since it is not that precise (usually +-10-15 ms)...maybe could use Unity unscaled time?
                string textToLog = $"{String.Format("{0:yyyy-MM-dd-HH-mm}", DateTime.Now)}{ColumnSeparator}{objectID}{ColumnSeparator}{entry}";

                logFile.WriteLine(textToLog);
                logFile.Flush();
                if (logToConsole || LogToConsole) // logToConsole parameter should be removed
                    Debug.Log("Log entry by user with ID " + objectID + ": " + entry);
            }
            else
            {
                Debug.LogWarning("Logger is not initialized - entry will not be added to log file.");
            }
        }

        /// <summary>
        /// Puts together a player identifier from NetworkConnection, use for human-readable logging.
        /// </summary>
        /// <param name="conn">Player's network connection</param>
        /// <returns>Player identifier string.</returns>
        public static string GetNameFromConnection(NetworkConnection conn)
        {
            // open to changes
            var player = conn.identity.GetComponent<UVRN_Player>();
            if (player == null)
                return "Unknown player";

            return (player.Role + ": " + player.Username + ", " + player.ID).Replace('\n', ' ');
        }

        public override void OnApplicationQuit()
        {
            base.OnApplicationQuit();
            Close();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            Close();
        }

        /// <summary>
        /// Finish logging.
        /// </summary>
        public void Close()
        {
            if (logFile != null)
            {
                Debug.Log("Closing log file");
                LogEntry("Server", "Session ended");

                logFile.Close();
                logFile = null;
            }
        }
        #endregion
    }
}
