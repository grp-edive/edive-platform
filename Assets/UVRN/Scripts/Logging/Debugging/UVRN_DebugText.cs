// Created by Vojtech Bruza
using TMPro;
using UnityEngine;

namespace UVRN.Logging.Debugging
{
    public class UVRN_DebugText : MonoBehaviour
    {
        public UVRN_UIDebugLogger manager;

        [SerializeField]
        private TextMeshProUGUI debugTextUI;

        private void OnEnable()
        {
            SetText(manager.debugText);
        }

        public void SetText(string text)
        {
            debugTextUI.SetText(text);
        }
    }
}
