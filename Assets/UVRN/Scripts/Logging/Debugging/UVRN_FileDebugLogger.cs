// Created by Vojtech Bruza
using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace UVRN.Logging.Debugging
{
    /// <summary>
    /// Responsible for logging errors to a file. (On a server or a client)
    /// </summary>
    public class UVRN_FileDebugLogger : MonoBehaviour
    {
        [SerializeField]
        private bool logNormalLog = false;
        [SerializeField]
        private bool logErrors = true;
        [SerializeField]
        private bool logAsserts = true;
        [SerializeField]
        private bool logWarnings = true;
        [SerializeField]
        private bool logExceptions = true;
        [SerializeField]
        private string fileName = "DebugLog.txt";

        private string logPath;

        private void OnEnable()
        {
            Init();
            Application.logMessageReceived += OnLogMessageReveived;
        }

        private void OnDisable()
        {
            Application.logMessageReceived -= OnLogMessageReveived;
        }

        private void Init()
        {
#if UNITY_EDITOR
            var logFolder = "_NonVersioned/Logs/Debug";
#elif UNITY_ANDROID || UNITY_IOS //&& !UNITY_EDITOR
            var logFolder = Application.persistentDataPath + "/" + Application.productName + "/Logs/Debug";
#else
            var logFolder = "Logs/Debug";
#endif

            string path = logFolder + "/" + String.Format("{0:yyyy-MM-dd-HH-mm}", DateTime.Now);

            //create directory if it don't exists already
            Directory.CreateDirectory(path);

            logPath = path + "/" + fileName;
            Debug.Log("File debug logger initialized in " + logPath);
        }

        private void OnLogMessageReveived(string logString, string stackTrace, LogType type)
        {
            switch (type)
            {
                case LogType.Assert:
                    if (!logAsserts) return;
                    break;
                case LogType.Error:
                    if (!logErrors) return;
                    break;
                case LogType.Warning:
                    if (!logWarnings) return;
                    break;
                case LogType.Exception:
                    if (!logExceptions) return;
                    break;
                case LogType.Log:
                    if (!logNormalLog) return;
                    break;
                default:
                    // return not break
                    return;
            }
            LogMessage($"{type}{Environment.NewLine}Message: {logString}{Environment.NewLine}StackTrace: {stackTrace}");
        }

        private void LogMessage(string message)
        {
            string textToLog = $"{String.Format("{0:HH:mm:ss}", DateTime.Now)} {message}";

            // does not have to await
            var task = AppendLine(textToLog);
            // TODO what if I start to write before I finish the previous? do I need a queue?
        }

        // TODO does this need to be async?
        private async Task AppendLine(string line)
        {
            using (StreamWriter file = new StreamWriter(logPath, append: true))
            {
                await file.WriteLineAsync(line);
            }
        }
    }
}
