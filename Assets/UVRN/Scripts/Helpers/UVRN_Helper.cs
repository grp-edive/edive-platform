﻿// Created by Vojtech Bruza
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UVRN.Helpers
{
    public static class UVRN_Helper
    {
        public static void ChangeLayersRecursively(this Transform trans, string layerName)
        {
            trans.gameObject.layer = LayerMask.NameToLayer(layerName);
            foreach (Transform child in trans)
            {
                child.ChangeLayersRecursively(layerName);
            }
        }

        [Obsolete("Is it working tho?")]
        public static void AddListenerOnce(UnityEvent target, UnityAction action)
        {
            UnityAction actionWrapper = null;
            actionWrapper = () =>
            {
                action();
                target.RemoveListener(actionWrapper);
            };
            target.AddListener(actionWrapper);
        }


    }
}
