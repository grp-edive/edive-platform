// Created by Vojtech Bruza
using Mirror;
using UnityEngine.Events;

namespace UVRN.Helpers
{
    public class UVRN_OnStart : NetworkBehaviour
    {
        public UnityEvent ClientStart = new UnityEvent();
        public UnityEvent ServerStart = new UnityEvent();

        public override void OnStartClient()
        {
            ClientStart.Invoke();
        }

        public override void OnStartServer()
        {
            ServerStart.Invoke();
        }
    }
}
