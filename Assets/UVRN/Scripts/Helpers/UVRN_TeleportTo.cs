// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace UVRN.Helpers
{
    public class UVRN_TeleportTo : MonoBehaviour
    {
        public XRRig xrRig;

        public Transform target;

        public void TeleportTo(Vector3 position)
        {
            if (xrRig == null)
            {
                Debug.LogWarning("No rig assigned, this might be slow.");
                xrRig = FindObjectOfType<XRRig>();
                if (xrRig == null)
                {
                    Debug.LogError("No rig found.");
                    return;
                }
            }
            xrRig.transform.position = position;

            // TODO use some smarter way to take the offset into account:
            // E.g. this?
            //var request = new TeleportRequest()
            //{
            //    destinationPosition = position
            //};
            //xrRig.GetComponent<TeleportationProvider>().QueueTeleportRequest(request);
        }

        public void TeleportTo(Transform t)
        {
            TeleportTo(t.position);
        }

        public void TeleportToTarget()
        {
            if (target == null) target = transform;

            TeleportTo(target.position);
        }
    }
}
