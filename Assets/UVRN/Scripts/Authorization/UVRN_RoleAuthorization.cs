// Created by Vojtech Bruza
using Mirror;
using System.Collections.Generic;
using UnityEngine;
using UVRN.Player;

namespace UVRN.Authorization
{
    public class UVRN_RoleAuthorization : NetworkBehaviour
    {
        // the role that allows averyone to interact with this object
        private static readonly string everyone = "everyone";

        [Tooltip("Should the roles be checked at all?")]
        public bool checkRoleAuthorization = true;

        [SerializeField]
        [Tooltip("Completely hide the objects that the user cannot interact with for the given client")]
        private bool disableUnauthorizedObjectsOnClient = true;

        [SerializeField]
        [Tooltip("Roles that are allowed to interact with this object (not synced if modified). Add role 'everyone' to enable interaction to everyone")]
        // TODO be able to dynamically change the roles for objects and sync with clients (use SyncHashSet?) - also change the local check on client then
        private List<string> allowedRoles = new List<string>();

        [SerializeField]
        [Tooltip("The objects that should be disabled for the unauthorized client. If this list is empty, will disable the child objects instead." +
            " This gameobject cannot be disabled since that would stop the network communication.")]
        private GameObject[] objectsToDisable;

        /// <summary>
        /// Callback called when the authorization check is finished on the server
        /// </summary>
        /// <param name="canInteract">If the user with given role is allowed to interact with the object</param>
        public delegate void AuthCallback(bool canInteract);

        /// <summary>
        /// Dictionary of all callbacks that should be called when the validation is copmleted on the server
        /// </summary>
        private Dictionary<string, AuthCallback> roleCallbacks = new Dictionary<string, AuthCallback>();

        /// <summary>
        /// Validates the interaction on the server (also on client if neccessary)
        /// </summary>
        /// <param name="role">Role to validate the authorization for</param>
        /// <param name="callback">Called on the client when the validation is completed on the server</param>
        /// <param name="checkOnClient">Should check also on the calling client?</param>
        public void CanInteract_CallCmd(string role, AuthCallback callback, bool checkOnClient = false)
        {
            // local check on client (list af allowed roles must be the same on the client for this to make sense):
            if (checkOnClient && !CanInteract(role))
            {
                // not valid even on the calling on client
                callback(false);
            }
            else
            {
                // check on the server
                roleCallbacks.Add(role, callback);
                Cmd_CanInteract(role);
            }
        }

        [Command(requiresAuthority = false)]
        private void Cmd_CanInteract(string role, NetworkConnectionToClient sender = null)
        {
            // check the role on the server
            TargetRpc_CanInteract(sender, role, CanInteract(role));
        }

        [TargetRpc]
        private void TargetRpc_CanInteract(NetworkConnection sender, string role, bool canInteract)
        {
            roleCallbacks.TryGetValue(role, out AuthCallback callback);
            callback(canInteract); // call the callback on the targated client
        }

        /// <summary>
        /// Just simple validation with no networking.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool CanInteract(string role)
        {
            // TODO make some levels of authorization to be able to customize the roles somehow in builds?
            // if the role check is disabled or allowed roles contains the given role or it contains the "everyone" role
            var can = !checkRoleAuthorization || allowedRoles.Contains(role) || allowedRoles.Contains(everyone);
            if (!can)
            {
                string roles = "";
                if (allowedRoles.Count > 0)
                {
                    roles = allowedRoles[0];
                    for (int i = 1; i < allowedRoles.Count; i++)
                    {
                        roles += $", {allowedRoles[i]}";
                    }
                }
                Debug.Log($"Role {role}, cannot interact with {gameObject.name}. Requires one of the following: {roles}");
                return false;
            }
            return true;
        }

        public bool CanInteract()
        {
            return CanInteract(UVRN_PlayerProfileManager.LocalProfile.role);
        }


        #region Mirror overrides
        public override void OnStartClient()
        {
            if (disableUnauthorizedObjectsOnClient && !CanInteract(UVRN_PlayerProfileManager.LocalProfile.role))
            {
                if (objectsToDisable == null || objectsToDisable.Length == 0)
                {
                    Debug.LogWarning($"{nameof(disableUnauthorizedObjectsOnClient)} is set to true but the list to disable is empty. Is this intentional?");
                    foreach(Transform child in gameObject.transform)
                    {
                        child.gameObject.SetActive(false);
                    }
                    return;
                }
                foreach (var go in objectsToDisable)
                {
                    go.SetActive(false);
                }
                return;
            }
        }
        #endregion
    }
}
