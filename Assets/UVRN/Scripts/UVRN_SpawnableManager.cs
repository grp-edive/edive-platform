﻿// Created by Vojtech Bruza
using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace UVRN
{
    public class UVRN_SpawnableManager : NetworkBehaviour
    {
        private UVRN_NetworkManager manager;
        [SerializeField]
        private GameObject spawnablePrefab;
        [SerializeField]
        private int objectsToSpawn = 1;
        // Server
        private List<GameObject> spawnedObjects = new List<GameObject>();

        public override void OnStartServer()
        {
            manager = (NetworkManager.singleton as UVRN_NetworkManager);

            SpawnObjects();
        }

        private void SpawnObjects()
        {
            for (int i = 0; i < objectsToSpawn; ++i)
            {
                spawnedObjects.Add(SpawnObject());
            }
        }
        
        private GameObject SpawnObject()
        {
            var spawnedGO = Instantiate(spawnablePrefab);
            NetworkServer.Spawn(spawnedGO);
            return spawnedGO;
        }

        private void DestroyObject(GameObject go)
        {
            if (go == null)
                return;

            NetworkServer.Destroy(go);
            Destroy(go); // ?
            spawnedObjects.Remove(go); // ?
        }
    }
}
