// GENERATED AUTOMATICALLY FROM 'Assets/EdiveCore/InputActions/Edive Desktop Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @EdiveDesktopControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @EdiveDesktopControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Edive Desktop Controls"",
    ""maps"": [
        {
            ""name"": ""Desktop Camera"",
            ""id"": ""84f7e5c3-6cc9-43d4-aa65-e3237e00d8f2"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""cab8ed1c-afd0-4dd5-8ce8-4ae0664ee521"",
                    ""expectedControlType"": ""Dpad"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LeftMouse"",
                    ""type"": ""Button"",
                    ""id"": ""5a7882e1-12ed-4bec-b17b-e484dc328526"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MuteMic"",
                    ""type"": ""Button"",
                    ""id"": ""b17a9305-fdbc-4a1f-b45a-e2cb8f0e9b7f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Axes"",
                    ""id"": ""2cd83c46-6d64-4fb1-83f2-21e03838bba6"",
                    ""path"": ""2DVector(mode=1)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Dekstop"",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Up"",
                    ""id"": ""70ed53ed-b2da-4ad2-835f-860bd9642ca0"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Dekstop"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Down"",
                    ""id"": ""b8afeed1-c5a5-4941-b229-07a767d465af"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Dekstop"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Left"",
                    ""id"": ""fbe4ed38-6197-477c-b922-1a91b32e7088"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Dekstop"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Right"",
                    ""id"": ""b6fbe7cd-0d74-406d-9319-f0e164e58f3b"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Dekstop"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""c2ada6d1-05d8-4339-9e28-223c86114f53"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Dekstop"",
                    ""action"": ""LeftMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bce9ee76-aa5a-4956-a72f-2fe567ecf1d9"",
                    ""path"": ""<Keyboard>/m"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Dekstop"",
                    ""action"": ""MuteMic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Dekstop"",
            ""bindingGroup"": ""Dekstop"",
            ""devices"": [
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Desktop Camera
        m_DesktopCamera = asset.FindActionMap("Desktop Camera", throwIfNotFound: true);
        m_DesktopCamera_Move = m_DesktopCamera.FindAction("Move", throwIfNotFound: true);
        m_DesktopCamera_LeftMouse = m_DesktopCamera.FindAction("LeftMouse", throwIfNotFound: true);
        m_DesktopCamera_MuteMic = m_DesktopCamera.FindAction("MuteMic", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Desktop Camera
    private readonly InputActionMap m_DesktopCamera;
    private IDesktopCameraActions m_DesktopCameraActionsCallbackInterface;
    private readonly InputAction m_DesktopCamera_Move;
    private readonly InputAction m_DesktopCamera_LeftMouse;
    private readonly InputAction m_DesktopCamera_MuteMic;
    public struct DesktopCameraActions
    {
        private @EdiveDesktopControls m_Wrapper;
        public DesktopCameraActions(@EdiveDesktopControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_DesktopCamera_Move;
        public InputAction @LeftMouse => m_Wrapper.m_DesktopCamera_LeftMouse;
        public InputAction @MuteMic => m_Wrapper.m_DesktopCamera_MuteMic;
        public InputActionMap Get() { return m_Wrapper.m_DesktopCamera; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DesktopCameraActions set) { return set.Get(); }
        public void SetCallbacks(IDesktopCameraActions instance)
        {
            if (m_Wrapper.m_DesktopCameraActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnMove;
                @LeftMouse.started -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnLeftMouse;
                @LeftMouse.performed -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnLeftMouse;
                @LeftMouse.canceled -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnLeftMouse;
                @MuteMic.started -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnMuteMic;
                @MuteMic.performed -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnMuteMic;
                @MuteMic.canceled -= m_Wrapper.m_DesktopCameraActionsCallbackInterface.OnMuteMic;
            }
            m_Wrapper.m_DesktopCameraActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @LeftMouse.started += instance.OnLeftMouse;
                @LeftMouse.performed += instance.OnLeftMouse;
                @LeftMouse.canceled += instance.OnLeftMouse;
                @MuteMic.started += instance.OnMuteMic;
                @MuteMic.performed += instance.OnMuteMic;
                @MuteMic.canceled += instance.OnMuteMic;
            }
        }
    }
    public DesktopCameraActions @DesktopCamera => new DesktopCameraActions(this);
    private int m_DekstopSchemeIndex = -1;
    public InputControlScheme DekstopScheme
    {
        get
        {
            if (m_DekstopSchemeIndex == -1) m_DekstopSchemeIndex = asset.FindControlSchemeIndex("Dekstop");
            return asset.controlSchemes[m_DekstopSchemeIndex];
        }
    }
    public interface IDesktopCameraActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLeftMouse(InputAction.CallbackContext context);
        void OnMuteMic(InputAction.CallbackContext context);
    }
}
