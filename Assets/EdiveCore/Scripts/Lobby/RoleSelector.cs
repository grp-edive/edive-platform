// Created by Vojtech Bruza
using UnityEngine;
using UVRN.Player;

namespace Edive.Lobby
{
    public class RoleSelector : MonoBehaviour
    {
        [SerializeField]
        private TextMesh passwordInputText;

        private void OnEnable()
        {
            passwordInputText.text = "";
        }

        public void SetPlayerRole(string role)
        {
            UVRN_PlayerProfileManager.LocalProfile.role = role;
        }

        public void SetPlayerPassword()
        {
            UVRN_PlayerProfileManager.LocalProfile.password = passwordInputText.text;
        }
    }
}
