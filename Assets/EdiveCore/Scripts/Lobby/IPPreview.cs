// Created by Vojtech Bruza
using Edive.Networking;
using UnityEngine;

namespace Edive.Lobby
{
    public class IPPreview : MonoBehaviour
    {
        [SerializeField]
        private TextMesh text;

        private Edive_NetworkManager m_netManager;
        private ushort defaultPort;

        private Edive_NetworkManager NetworkManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        private void Start()
        {
            defaultPort = NetworkManager.Port;
        }

        private void Update()
        {
            string t = NetworkManager.networkAddress;
            ushort port = NetworkManager.Port;
            if (port != defaultPort)
            {
                t += ":" + port;
            }
            text.text = t;
        }
    }
}
