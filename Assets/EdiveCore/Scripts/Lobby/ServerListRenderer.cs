﻿// Created by Vojtech Bruza
using Edive.Networking;
using Edive.Networking.HTTP.ServerList;
using Mirror;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Edive.Lobby
{
    public class ServerListRenderer : MonoBehaviour
    {
        public GameObject buttonPrefab;

        public string FetchingMessage = "Fetching server list...";
        public string NoServersMessage = "No servers found";
        //public string ConnectingMessage = "Connecting to server...";
        //public string FailedMessage = "Connection failed";

        public Vector3 offest;

        private Edive_NetworkManager m_netManager;
        private Edive_NetworkManager NetworkManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        public TextMesh statusText;

        private List<ServerInfo> fetchedServers;

        private ushort currentPageIndex = 0;
        public ushort buttonsPerPage = 5;
        private bool nextPageAvailable = true;

        public GameObject prevButton; // TODO show hide
        public GameObject nextButton;

        public UnityEvent OnServerSelected = new UnityEvent();

        public void Clicked(string address, string port)
        {
            NetworkManager.networkAddress = address;
            NetworkManager.Port = ushort.Parse(port);
            OnServerSelected.Invoke();
        }

        protected void OnEnable()
        {
            //NetworkManager.Client_OnStart.AddListener(ConnectingStarted);
            //NetworkManager.Client_OnDisconnectFromServer.AddListener(ConnectingFailed);

            Refresh();
        }

        private void OnDisable()
        {
            //NetworkManager.Client_OnStart.RemoveListener(ConnectingStarted);
            //NetworkManager.Client_OnDisconnectFromServer.RemoveListener(ConnectingFailed);
        }

        public void Refresh()
        {
            SetMessage(FetchingMessage);

            NetworkManager.GetServerList(GetServerListCallback);
        }
        
        private void GetServerListCallback(List<ServerInfo> servers)
        {
            fetchedServers = servers;

            // if should show empty page
            if (fetchedServers.Count < currentPageIndex * buttonsPerPage)
            {
                currentPageIndex = 0;
            }

            UpdateVis();
        }

        private void UpdateVis()
        {
            // Remove all old children
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            if (fetchedServers.Count == 0)
            {
                SetMessage(NoServersMessage);
                return;
            }

            statusText?.gameObject.SetActive(false);
            RemoveChildren();

            var position = new Vector3(0, 0, 0);

            int startIndex = buttonsPerPage * currentPageIndex;
            int endIndex = startIndex + buttonsPerPage;

            // buttons
            if (endIndex >= fetchedServers.Count)
            {
                nextPageAvailable = false;
                nextButton?.SetActive(false);
            }
            else
            {
                nextPageAvailable = true;
                nextButton?.SetActive(true);
            }

            if (startIndex <= 0)
            {
                prevButton?.SetActive(false);
            }
            else
            {
                prevButton?.SetActive(true);
            }

            for (int i = startIndex; i < endIndex; i++)
            {
                if (i >= fetchedServers.Count) return;

                ServerInfo server = fetchedServers[i];
                var button = Instantiate(buttonPrefab, transform.position, Quaternion.identity);
                button.transform.rotation = gameObject.transform.rotation;
                button.transform.position += position;
                button.transform.parent = gameObject.transform;

                ServerListButton script = button.GetComponent<ServerListButton>();
                if (script)
                {
                    script.SetData(server, this);
                }
                else
                {
                    Debug.LogError("Probably missing component: " + nameof(ServerListButton));
                }

                position += offest;
            }
        }

        public void NextPage()
        {
            if (nextPageAvailable) currentPageIndex++;
            Refresh();
        }

        public void PrevPage()
        {
            if (currentPageIndex > 0) currentPageIndex--;
            Refresh();
        }

        //private void ConnectingStarted()
        //{
        //    SetMessage(ConnectingMessage);
        //}

        //private void ConnectingFailed(NetworkConnection conn)
        //{
        //    SetMessage(FailedMessage);
        //}

        private void RemoveChildren()
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        private void SetMessage(string message)
        {
            RemoveChildren();
            statusText?.gameObject.SetActive(true);
            statusText.text = message;
        }
    }
}
