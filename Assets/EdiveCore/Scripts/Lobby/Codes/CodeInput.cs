// Created by Vojtech Bruza
using UnityEngine;
using UVRN;

namespace Edive.Lobby.Codes
{
    public class CodeInput : MonoBehaviour
    {
        public string Code = null;

        [SerializeField]
        private TextMesh textPreview = null;

        public UnityEventString onConfirm = new UnityEventString();

        // to be serialized in the editor
        public void SetCode(string code)
        {
            Code = code;
        }

        private void Update()
        {
            if (textPreview != null)
            {
                textPreview.text = Code;
            }
        }

        public void Confirm()
        {
            onConfirm.Invoke(Code);
        }
    }
}
