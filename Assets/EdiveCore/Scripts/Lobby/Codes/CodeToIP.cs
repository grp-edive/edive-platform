// Author Vojtech Bruza
using Edive.Networking;
using Edive.Networking.HTTP;
using UnityEngine;
using UVRN;

namespace Edive.Lobby.Codes
{
    public class CodeToIP : MonoBehaviour
    {
        private Edive_NetworkManager m_netManager;
        private Edive_NetworkManager NetworkManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        public bool autoConnectOnIPReceived = false;

        [SerializeField]
        private UVRN_ConnectToIP connectToIP = null;

        public void SetPlayerManagerIPFromCode(string code)
        {
            Debug.Log("Getting server from code: " + code);
            // TODO other orgs
            string org = "";
            NetworkManager.httpNetManager.GetServerByCode(org, code, GetSever_Callback);
        }

        private void GetSever_Callback(QueryServerResponse.Data serverData)
        {
            Debug.Log("Recieved server: " + serverData);
            NetworkManager.networkAddress = serverData.address;
            NetworkManager.Port = serverData.port;

            if (autoConnectOnIPReceived)
            {
                connectToIP.TryToConnect();
            }
        }
    }
}
