// Created by Vojtech Bruza
using UnityEngine;
using UVRN.Player;

namespace Edive.Lobby
{
    public class UsernameText : MonoBehaviour
    {
        [SerializeField]
        private TextMesh text;

        private void Awake()
        {
            text = GetComponent<TextMesh>();
        }

        private void Update()
        {
            if (text.text != UVRN_PlayerProfileManager.LocalProfile.username) text.text = UVRN_PlayerProfileManager.LocalProfile.username;
        }
    }
}
