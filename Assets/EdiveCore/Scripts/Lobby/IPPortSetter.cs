// Created by Vojtech Bruza
using Edive.Networking;
using System;
using UnityEngine;

namespace Edive.Lobby
{
    public class IPPortSetter : MonoBehaviour
    {
        private Edive_NetworkManager m_netManager;
        private Edive_NetworkManager NetworkManager
        {
            get
            {
                if (!m_netManager)
                {
                    m_netManager = (Edive_NetworkManager)Edive_NetworkManager.singleton;
                }
                return m_netManager;
            }
        }

        public void SetIP(string ip)
        {
            NetworkManager.networkAddress = ip;
        }

        public void SetPort(string port)
        {
            try
            {
                if (port.Length > 0) NetworkManager.Port = ushort.Parse(port);
            }
            catch (Exception)
            {
                Debug.LogError("Could not parse port (cannot be larger than 65535) " + port);
            }
        }

        public void SetIPPort(string ip_port)
        {
            string[] ipPortParsed = ip_port.Split(':');
            if (ipPortParsed.Length != 1 && ipPortParsed.Length != 2)
            {
                Debug.Log("Incorrect format for IP or port " + ip_port);
                return;
            }
            SetIP(ipPortParsed[0]);
            if (ipPortParsed.Length == 2)
            {
                string port = ipPortParsed[1];
                SetPort(port);
            }
        }

        public void SetIPPort(TextMesh textMesh)
        {
            SetIPPort(textMesh.text);
        }
    }
}