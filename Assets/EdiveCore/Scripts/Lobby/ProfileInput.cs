// Created by Vojtech Bruza
using UnityEngine;
using UVRN.Player;

namespace Edive.Lobby
{
    public class ProfileInput : MonoBehaviour
    {
        [SerializeField]
        private TextMesh nameInputText;

        private void OnEnable()
        {
            LoadPlayerName();
        }

        public void RandomName()
        {
            nameInputText.text = UVRN_PlayerProfileManager.GetRandomName();
        }

        public void SetPlayerName()
        {
            UVRN_PlayerProfileManager.LocalProfile.username = nameInputText.text;
        }

        public void LoadPlayerName()
        {
            nameInputText.text = UVRN_PlayerProfileManager.LocalProfile.username;
        }
    }
}
