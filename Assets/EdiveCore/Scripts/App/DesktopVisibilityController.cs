// Created by Vojtech Bruza
using System;
using UnityEngine;

namespace Edive.App
{
    public class DesktopVisibilityController : MonoBehaviour
    {
        [Serializable]
        public class VisibilitySet
        {
            public GameObject[] objects;
        }

        public VisibilitySet[] visibilitySets;

        [ContextMenu("Show all")]
        public void ShowAll()
        {
            ToggleVisibilityAll(true);
        }

        [ContextMenu("Hide all")]
        public void HideAll()
        {
            ToggleVisibilityAll(false);
        }

        public void ToggleVisibilityAll(bool visibility)
        {
            for (int i = 0; i < visibilitySets.Length; i++)
            {
                ToggleSetVisibility(i, visibility);
            }
        }

        public void ToggleSetVisibility(int index, bool visibility)
        {
            VisibilitySet set = visibilitySets[index];
            foreach (var go in set.objects)
            {
                go.SetActive(visibility);
            }
        }
    }
}
