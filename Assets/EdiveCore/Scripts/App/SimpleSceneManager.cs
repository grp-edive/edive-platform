﻿// Created by Vojtech Bruza
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Edive.App
{
    public class SimpleSceneManager : MonoBehaviour
    {
        public void LoadScene(string scene)
        {
            StartCoroutine(LoadSceneAdditiveCoroutine(scene));
        }

        private IEnumerator LoadSceneAdditiveCoroutine(string sceneName)
        {
            var op = SceneManager.LoadSceneAsync(sceneName);
            yield return op;
            Debug.Log($"Scene loaded: {sceneName}");
        }
    }
}
