// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.InputSystem;

namespace Edive.App.Controls
{
    /// <summary>
    /// Should be updated to use input action, not direct input.
    /// </summary>
    public class DesktopPivotCameraController : MonoBehaviour
    {
#if !UNITY_SERVER
        [SerializeField]
        private Transform cameraRotatorPivot;
        [SerializeField]
        private Transform cameraTransform;

        [Range(0, 180)]
        [Tooltip("Limit the botom camera position. 0 means from bottom up. 180 from top down.")]
        public float minPitch = 80;
        [Range(0, 180)]
        [Tooltip("Limit the top camera position. 0 means from bottom up. 180 from top down.")]
        public float maxPitch = 170;

        [Range(0.001f, 10)]
        [Tooltip("Limit the camera zoom.")]
        public float minDistance = 0.1f;
        [Range(0.001f, 20)]
        [Tooltip("Limit the camera zoom.")]
        public float maxDistance = 5;

        public bool LimitMovementToBounds = false;

        public Collider bounds;

        public float mouseRotateSensitivity = 0.05f;
        public float mouseTranslateSensitivity = 1f;
        public float mouseScrollSensitivity = 0.1f;
        public bool invertY = false;

        public AnimationCurve mouseSensitivityCurve = new AnimationCurve(new Keyframe(0f, 0.5f, 0f, 5f), new Keyframe(1f, 2.5f, 0f, 0f));

        private float currentPitch;
        private float currentZoom;

        [SerializeField]
        private bool moveActive = true;
        [SerializeField]
        private bool rotateActive = true;
        [SerializeField]
        private bool scaleActive = true;

        public void SetActiveMove(bool active)
        {
            moveActive = active;
        }

        public void SetActiveRotate(bool active)
        {
            rotateActive = active;
        }

        public void SetActiveZoom(bool active)
        {
            scaleActive = active;
        }

        private void Reset()
        {
            cameraRotatorPivot = transform;
            cameraTransform = GetComponentInChildren<Camera>().transform;
        }

        private void Awake()
        {
            if (cameraRotatorPivot != cameraTransform.parent)
            {
                Debug.LogError("Camera rotator must be parent of the camera.");
            }
            if (LimitMovementToBounds && !bounds)
            {
                Debug.LogError("Bounds not set.");
            }

            // TODO fix starting orientation of the rotator, so it correctly translates the object
            
            // set initial zoom
            var dist = Vector3.Distance(cameraRotatorPivot.position, cameraTransform.position);
            if (Mathf.Approximately(dist, 0))
            {
                Debug.LogError("The camera is not far enough.");
                return;
            }
            else UpdateZoom(dist);

            // get initial pitch
            cameraTransform.LookAt(cameraRotatorPivot.position, Vector3.up);
            currentPitch = Vector3.Angle(Vector3.down, cameraTransform.position - cameraRotatorPivot.position);
        }

        private void LateUpdate()
        {
            // TODO keyboard rotation and controls should be mapped to input events
            // TODO use input events, not mouse directly

            if (Mouse.current == null) return;

            // rotate
            if (Mouse.current.rightButton.isPressed)
            {
                float x = Mouse.current.delta.x.ReadValue() * mouseRotateSensitivity;
                float y = Mouse.current.delta.y.ReadValue() * mouseRotateSensitivity;
                var mouseMovement = new Vector2(x, y * (invertY ? 1 : -1));
                var mouseSensitivityFactor = mouseSensitivityCurve.Evaluate(mouseMovement.magnitude);
                var scaledMovement = mouseSensitivityFactor * mouseMovement;
                if (rotateActive) UpdateCamRot(scaledMovement);
            }
            // move
            if (Mouse.current.leftButton.isPressed)
            {
                float x = Mouse.current.delta.x.ReadValue() * mouseTranslateSensitivity;
                float y = Mouse.current.delta.y.ReadValue() * mouseTranslateSensitivity;
                var mouseMovement = new Vector2(-x, -y);
                var mouseSensitivityFactor = mouseSensitivityCurve.Evaluate(mouseMovement.magnitude);
                // scale also by current zoom
                var scaledMovement = mouseSensitivityFactor * mouseMovement * currentZoom;
                if (moveActive) UpdateCamPos(scaledMovement);
            }
            // zoom
            float mouseScrollDeltaY = Mouse.current.scroll.y.ReadValue();
            if (mouseScrollDeltaY != 0)
            {
                if (scaleActive) UpdateZoom(mouseScrollDeltaY * mouseScrollSensitivity * 0.01f);
            }
        }

        private void UpdateCamPos(Vector2 scaledMovement)
        {
            var posDiff = new Vector3(scaledMovement.x / 100, 0, scaledMovement.y / 100);
            // alternative to translate but does not apply the new position to the transform:
            var newPos = cameraRotatorPivot.position + cameraRotatorPivot.TransformDirection(posDiff);
            // check if the new position is within the bounds
            if (!LimitMovementToBounds || (bounds != null && bounds.bounds.Contains(newPos))) cameraRotatorPivot.position = newPos;
        }

        private void UpdateCamRot(Vector2 diff)
        {
            // yaw
            cameraRotatorPivot.Rotate(Vector3.up, diff.x);
            // pitch (clamping and then using the difference)
            var newPitch = Mathf.Clamp(currentPitch + diff.y, minPitch, maxPitch);
            var pitchDiff = newPitch - currentPitch;
            cameraTransform.RotateAround(cameraRotatorPivot.position, cameraTransform.right, pitchDiff);
            currentPitch = newPitch;
        }

        private void UpdateZoom(float amount)
        {
            var newPos = amount * cameraTransform.forward + cameraTransform.position;
            currentZoom = Vector3.Distance(cameraRotatorPivot.position, newPos);
            // add 0.001f to prevent floating point error
            if (currentZoom < minDistance + 0.001f || currentZoom > maxDistance - 0.001f)
            {
                return;
            }
            // now just check if the target is not behind the camera after zooming too close
            if (Vector3.Dot(cameraTransform.forward, cameraRotatorPivot.position - newPos) >= 0)
            {
                cameraTransform.position = newPos;
            }
        }
#endif
    }
}
