// Created by Vojtech Bruza
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UVRN;
using UVRN.VoiceChat;

namespace Edive.App.Controls
{
    public class Edive_DesktopControls : UVRN_TrackeableControls
    {
        private UVRN_NetworkManager m_NetworkManager;
        private UVRN_NetworkManager NetworkManager
        {
            get
            {
                if (!m_NetworkManager)
                {
                    m_NetworkManager = (UVRN_NetworkManager)UVRN_NetworkManager.singleton;
                }
                if (m_NetworkManager != UVRN_NetworkManager.singleton)
                {
                    Debug.LogError("Net manager instance changed.");
                }
                return m_NetworkManager;
            }
        }

        // TODO use InputActionAsset?
        public EdiveDesktopControls Controls { get; private set; }

        [SerializeField]
        private GameObject unmutedIcon;

        [SerializeField]
        private Transform mainCamera;


        [SerializeField]
        private bool muteOnConnect = true;

        //[SerializeField]
        //private DesktopCameraController cameraController;

        public DesktopVisibilityController visibilityController;

        public char toggleVisibilityCharacter = 'v';

        private int visibilitySetIndex = 0;
        private bool initialized = false;

        public override Transform GetHead()
        {
            return mainCamera;
        }

        public override Transform GetLeftHand()
        {
            return null;
        }

        public override Transform GetRightHand()
        {
            return null;
        }

        private void Awake()
        {
            Init();
        }
        private void Start()
        {
            if (NetworkManager?.VoiceChatManager != null) NetworkManager.VoiceChatManager.MuteMicOnConnect = muteOnConnect;
        }

        private void Init()
        {
            if (initialized) return;
            
            Controls = new EdiveDesktopControls();

            initialized = true;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            // set active can be called from another scripts awake?
            Init();

            if (UnityEngine.InputSystem.Keyboard.current != null) UnityEngine.InputSystem.Keyboard.current.onTextInput += OnTextInput;
            Controls.Enable();

            Controls.DesktopCamera.MuteMic.performed += MuteMic;
        }

        protected override void OnDisable()
        {
            Controls.Disable();
            if (UnityEngine.InputSystem.Keyboard.current != null) UnityEngine.InputSystem.Keyboard.current.onTextInput -= OnTextInput;
            base.OnDisable();

            Controls.DesktopCamera.MuteMic.performed -= MuteMic;
        }

        private void OnTextInput(char c)
        {
            if (char.ToLower(c) == char.ToLower(toggleVisibilityCharacter) && visibilityController)
            {
                visibilityController.ToggleSetVisibility(visibilitySetIndex, true);
                visibilitySetIndex = (visibilitySetIndex + 1) % visibilityController.visibilitySets.Length;
                visibilityController.ToggleSetVisibility(visibilitySetIndex, false);
            }
        }

        private void MuteMic(InputAction.CallbackContext context)
        {
            NetworkManager.VoiceChatManager.SetMicMuted(!NetworkManager.VoiceChatManager.IsMicMuted());
            unmutedIcon.SetActive(!NetworkManager.VoiceChatManager.IsMicMuted());
        }
    }
}
