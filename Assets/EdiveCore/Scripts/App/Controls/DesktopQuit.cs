// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.InputSystem;

namespace Edive.App.Controls
{
    public class DesktopQuit : MonoBehaviour
    {
#if !UNITY_SERVER
        private void Update()
        {
            // TODO using events
            if (Keyboard.current == null) return;
            if (Keyboard.current.escapeKey.wasPressedThisFrame)
            {
                Application.Quit();
            }
        }
#endif
    }
}
