// Created by Vojtech Bruza

using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Edive.App
{
    public class SceneLoader : MonoBehaviour
    {
        [Scene]
        public string SceneToLoad;

        public void LoadScene()
        {
            SceneManager.LoadScene(SceneToLoad);
        }
    }
}
