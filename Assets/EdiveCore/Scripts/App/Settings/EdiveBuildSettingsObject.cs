// Created by Vojtech Bruza
using UnityEngine;
using static UVRN.UVRN_NetworkManager;


namespace Edive.App.Settings
{
    [CreateAssetMenu(fileName = "Edive Build Settings", menuName = "Edive/New Build Settings", order = 4)]
    public class EdiveBuildSettingsObject : ScriptableObject
    {
        public ConnectionType connectionType = ConnectionType.Client;

        public EdiveSettingsObject.TargetPlatform targetPlatform = EdiveSettingsObject.TargetPlatform.VR;

        /// <summary>
        /// Whether the server should be listed in the client release builds.
        /// </summary>
        public bool isDev = true;

        public string[] clientOnlyPackages;
    }
}
