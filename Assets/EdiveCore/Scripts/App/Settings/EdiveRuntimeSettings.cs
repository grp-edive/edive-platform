// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.App.Settings
{
    [CreateAssetMenu(fileName = "Edive Runtime Settings", menuName = "Edive/New Runtime Settings", order = 0)]
    public class EdiveRuntimeSettings : ScriptableObject
    {
        // (#secret)
        // Must be embeded to the build, because it cannot simply be changed for the Android client build
        public string serverManagerUrl = "";

        // (#secret)
        // Must be embeded to the build, because it cannot simply be changed for the Android client build
        public string serverListingManagerUrl = "";

        // (#secret)
        public string serverListingManagerSecret = "";
    }
}
