﻿// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.App.Settings
{
    [CreateAssetMenu(fileName = "Edive General Settings", menuName = "Edive/New General Settings", order = 1)]
    public class EdiveSettingsObject : ScriptableObject
    {
        public enum TargetPlatform
        {
            Desktop,
            VR
        };
        // app specific setup (secrets)
        public EdiveRuntimeSettings RuntimeSettings;
        public EdiveBuildSettingsObject BuildSettings;
        public EdiveServerSettingsObject ServerSettings;
        public EdiveClientSettingsObject ClientSettings;
        public string MainScenePath = "Assets/EdiveCore/Scenes/EDIVE_Main.unity";
    }
}
