// Created by Vojtech Bruza
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Edive.App.Settings
{
    #region Settings File Properties
    public abstract class FileSettingsProperty<T>
    {
        public string key;
        public string tooltip;
        public T defaultValue;

        public virtual string Serialize(string separator)
        {
            return key + separator + ToString();
        }
        public abstract T Deserialize(string s);
    }

    public class FileSettingsPropertyString : FileSettingsProperty<string>
    {
        public override string ToString()
        {
            return defaultValue.ToString();
        }
        public override string Deserialize(string s)
        {
            return s.Trim();
        }
    }

    public class FileSettingsPropertyBool : FileSettingsProperty<bool>
    {
        public override string ToString()
        {
            if (defaultValue)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
        public override bool Deserialize(string s)
        {
            s = s.Trim();
            if (s == "1")
            {
                return true;
            }
            else if (s == "0")
            {
                return false;
            }
            else throw new IOException();
        }
    }
    public class FileSettingsPropertyInt : FileSettingsProperty<int>
    {
        public override string ToString()
        {
            return defaultValue.ToString();
        }

        public override int Deserialize(string s)
        {
            return int.Parse(s.Trim());
        }
    }
    public class FileSettingsPropertyListString : FileSettingsProperty<List<string>>
    {
        public readonly char valuesSeparator = ',';
        public override string ToString()
        {
            if (defaultValue.Count > 0)
            {
                var s = defaultValue[0];
                for (int i = 1; i < defaultValue.Count; i++)
                {
                    s += valuesSeparator + defaultValue[i];
                }
                return s;
            }
            else return "";
        }

        public override List<string> Deserialize(string s)
        {
            var values = new List<string>();
            var valuesParsed = s.Split(valuesSeparator);
            foreach (var v in valuesParsed)
            {
                values.Add(v.Trim());
            }
            return values;
        }
    }
    public class FileSettingsPropertyDictionaryString : FileSettingsProperty<Dictionary<string, string>>
    {
        public readonly char keyValueSeparator = ':';
        public readonly char pairSeparator = ',';
        public override string ToString()
        {
            if (defaultValue.Count > 0)
            {
                var s = "";
                int i = 0;
                foreach (var kvp in defaultValue)
                {
                    var kvs = kvp.Key + keyValueSeparator + kvp.Value;
                    if (i == 0) s += kvs;
                    else s += pairSeparator + kvs;
                    i++;
                }
                return s;
            }
            else return "";
        }

        public override Dictionary<string, string> Deserialize(string s)
        {
            var keyValuePairs = new Dictionary<string, string>();
            var keyValuePairsParsed = s.Split(pairSeparator);
            foreach (var kvp in keyValuePairsParsed)
            {
                var keyValueSplitted = kvp.Split(keyValueSeparator);
                if (keyValueSplitted.Length == 2) keyValuePairs[keyValueSplitted[0].Trim()] = keyValueSplitted[1].Trim();
            }
            return keyValuePairs;
        }
    }
    #endregion

    /// <summary>
    /// Able to load settings from file. But must be manually called (LoadServerSettingsFromFile)
    /// </summary>
    [CreateAssetMenu(fileName = "Edive Server Settings", menuName = "Edive/New Server Settings", order = 2)]
    public class EdiveServerSettingsObject : ScriptableObject
    {
        private FileSettings ediveFileSettings = new FileSettings();
        private FileSettings ediveRolesFileSettings = new FileSettings();

        public string ServerSettingsFileName = "server.txt";
        public string ServerRolesFileName = "roles.txt";

        public FileSettingsPropertyString serverTitle = new FileSettingsPropertyString()
        {
            key = "server_title",
            defaultValue = "EDive Server",
            tooltip = "Will be used to register the server and also displayed as the server name in the lobby"
        };
        public FileSettingsPropertyBool localIP = new FileSettingsPropertyBool()
        {
            key = "local_ip",
            defaultValue = false,
            tooltip = "Should the server send its local IP instead of a global one to register itself?"
        };
        public FileSettingsPropertyBool autoRegisterServerOnline = new FileSettingsPropertyBool()
        {
            key = "register_online",
            defaultValue = true,
            tooltip = "Should the server register itself in the server list"
        };
        public FileSettingsPropertyInt port = new FileSettingsPropertyInt()
        {
            key = "port",
            defaultValue = 53454,
            tooltip = "Server port"
        };
        public FileSettingsPropertyInt updateFrequency = new FileSettingsPropertyInt()
        {
            key = "update_frequency",
            defaultValue = 30,
            tooltip = "How often should server ping the manager (s)"
        };
        public FileSettingsPropertyDictionaryString rolesPasswordsDict = new FileSettingsPropertyDictionaryString()
        {
            key = "roles",
            defaultValue = new Dictionary<string, string>(),
            tooltip = "Allowed roles for the server with their passwords in the format 'role1:password1,role2:password2"
        };
        public FileSettingsPropertyBool enableLogging = new FileSettingsPropertyBool()
        {
            key = "enable_logging",
            defaultValue = true,
            tooltip = "Should the server log the events to a file"
        };
        public FileSettingsPropertyString serverCode = new FileSettingsPropertyString()
        {
            key = "server_code",
            defaultValue = "",
            tooltip = "Can be used to access the server from the lobby. If empty, will be auto-assigned. Use only these characters: 'AB0123456789'. Should be 4 chars long."
        };

        public string ServerTitle
        {
            get
            {
                var fromFile = ediveFileSettings.Get(serverTitle.key);
                if (fromFile != null) return fromFile;
                return serverTitle.defaultValue;
            }
        }

        public string ServerCode
        {
            get
            {
                var fromFile = ediveFileSettings.Get(serverCode.key);
                if (fromFile != null) return fromFile;
                return serverCode.defaultValue;
            }
        }

        public bool LocalIP
        {
            get
            {
                var fromFile = ediveFileSettings.Get(localIP.key);
                try
                {
                    return localIP.Deserialize(fromFile);
                }
                catch { }
                return localIP.defaultValue;
            }
        }

        public bool AutoRegisterServerOnline
        {
            get
            {
                var fromFile = ediveFileSettings.Get(autoRegisterServerOnline.key);
                try
                {
                    return autoRegisterServerOnline.Deserialize(fromFile);
                }
                catch { }
                return autoRegisterServerOnline.defaultValue;
            }
        }

        public ushort Port
        {
            get
            {
                var fromFile = ediveFileSettings.Get(port.key);
                try
                {
                    return (ushort)port.Deserialize(fromFile);
                }
                catch { }
                return (ushort)port.defaultValue;
            }
        }

        public int UpdateFrequency
        {
            get
            {
                var fromFile = ediveFileSettings.Get(updateFrequency.key);
                try
                {
                    return updateFrequency.Deserialize(fromFile);
                }
                catch { }
                return updateFrequency.defaultValue;
            }
        }

        public bool EnableLogging
        {
            get
            {
                var fromFile = ediveFileSettings.Get(enableLogging.key);
                try
                {
                    return enableLogging.Deserialize(fromFile);
                }
                catch { }
                return enableLogging.defaultValue;
            }
        }

        public Dictionary<string, string> Roles
        {
            get
            {
                // TODO use one file instead?
                // TODO or better - use the second file, but one line per role (directly save and get as the dictionary from the FileSettings)
                var fromFile = ediveRolesFileSettings.Get(rolesPasswordsDict.key);
                try
                {
                    return rolesPasswordsDict.Deserialize(fromFile);
                }
                catch { }
                return rolesPasswordsDict.defaultValue;
            }
        }

        public bool LoadServerRolesFromFile(Dictionary<string, string> defaultValueToFile)
        {
#if UNITY_EDITOR || !UNITY_ANDROID
            var path = "ServerSettings/" + ServerRolesFileName;
#if UNITY_EDITOR
            path = Application.dataPath + "/_NonVersioned/" + path;
#endif
            if (ediveRolesFileSettings.LoadFromFile(path))
            {
                Debug.Log($"Server roles loaded from file ({path})\n" + ediveRolesFileSettings.ToString());
                return true;
            }
            else
            {
                FileSettingsPropertyDictionaryString defaultRoles = new FileSettingsPropertyDictionaryString();
                defaultRoles.defaultValue = defaultValueToFile;
                var props = new string[][] {
                    new string[] { rolesPasswordsDict.key, defaultRoles.ToString(), rolesPasswordsDict.tooltip }
                };
                if (ediveRolesFileSettings.CreateFileWithProperties(path, props))
                {
                    Debug.Log($"Unable to load server roles from file. Using Default and creating a file.");
                }
                else
                {
                    Debug.Log($"Unable to create a file with server roles.");
                }
            }
#endif
            return false;
        }

        /// <summary>
        /// Will try to load server settings from file.
        /// Creates new file with default values, if there is none created.
        /// </summary>
        public void LoadServerSettingsFromFile()
        {
#if UNITY_EDITOR || !UNITY_ANDROID
            var path = "ServerSettings/" + ServerSettingsFileName;
#if UNITY_EDITOR
            path = Application.dataPath + "/_NonVersioned/" + path;
#endif
            // TODO right now it does not detect missing server settings
            if (ediveFileSettings.LoadFromFile(path))
            {
                Debug.Log($"Server settings loaded from file ({path})\n" + ediveFileSettings.ToString());

            }
            else
            {
                var props = new string[][] {
                    new string[]{ serverTitle.key, serverTitle.ToString(), serverTitle.tooltip },
                    new string[]{ localIP.key, localIP.ToString(), localIP.tooltip },
                    new string[]{ autoRegisterServerOnline.key, autoRegisterServerOnline.ToString(), autoRegisterServerOnline.tooltip },
                    new string[]{ port.key, port.ToString(), port.tooltip },
                    new string[]{ updateFrequency.key, updateFrequency.ToString(), updateFrequency.tooltip },
                    new string[]{ enableLogging.key, enableLogging.ToString(), enableLogging.tooltip },
                    new string[]{ serverCode.key, serverCode.ToString(), serverCode.tooltip },
                };
                if (ediveFileSettings.CreateFileWithProperties(path, props))
                {
                    Debug.Log($"Unable to load server settings from file. Using Default and creating a file.");
                }
                else
                {
                    Debug.Log($"Unable to create a file with server settings.");
                }
            }
#endif
        }
    }
}
