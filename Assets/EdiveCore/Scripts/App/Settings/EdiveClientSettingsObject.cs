// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.App.Settings
{
    [CreateAssetMenu(fileName = "Edive Client Settings", menuName = "Edive/New Client Settings", order = 3)]
    public class EdiveClientSettingsObject : ScriptableObject
    {
        public bool autoConnectClientOnServerFound = false;
    }
}
