﻿// Created by Vojtech Bruza
using Edive.Interactions.Highlighting;
using UnityEngine;

namespace Edive.UI
{
    public class SimpleToggleGroup : MonoBehaviour
    {
        [SerializeField]
        private Material materialWhenON;
        [SerializeField]
        private int initialOn = 0;
        [SerializeField]
        private MaterialSwapHighlighter[] toggles;

        private Material[] originalMaterials;

        public int LastSelectedToggleIndex { get; private set; }

        private void Awake()
        {
            originalMaterials = new Material[toggles.Length];
            for (int i = 0; i < toggles.Length; i++)
            {
                originalMaterials[i] = toggles[i].IdleMaterial;
            }

            Select(initialOn);
        }

        public void Select(int newToggleIndex)
        {
            if (newToggleIndex < 0 || newToggleIndex >= toggles.Length)
            {
                Debug.LogError("Trying to toggle element out of range.");
                return;
            }

            // deselect old
            toggles[LastSelectedToggleIndex].IdleMaterial = originalMaterials[LastSelectedToggleIndex];
            toggles[LastSelectedToggleIndex].Unhighlight(); // to show new idle material immediately

            // select new
            toggles[newToggleIndex].IdleMaterial = materialWhenON;
            toggles[newToggleIndex].Unhighlight(); // to show new idle material immediately

            // update index
            LastSelectedToggleIndex = newToggleIndex;
        }
    }
}
