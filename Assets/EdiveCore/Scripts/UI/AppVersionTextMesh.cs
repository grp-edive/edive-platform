﻿// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.UI
{
    [ExecuteInEditMode]
    public class AppVersionTextMesh : MonoBehaviour
    {
        public TextMesh versionText;
        public TextMesh unityVersionText;

        private void OnEnable()
        {
            UpdateText();
        }

        public void UpdateText()
        {
            if (versionText) versionText.text = Application.productName + " version " + Application.version;
            if (unityVersionText) unityVersionText.text = "Running on Unity version " + Application.unityVersion;
        }
    }
}
