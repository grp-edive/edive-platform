// Created by Vojtech Bruza
using UnityEditor;
using UnityEngine;

namespace Edive.UI.Keyboard
{
    public class KeyboardGenerator : MonoBehaviour
    {
#if UNITY_EDITOR
        public GameObject KeyPrefab;
        public Transform KeysParent;
        public string[] Rows;

        public float xOffset = 0.1f;
        public float zOffset = 0.1f;

        public void GenerateKeyboard()
        {
            if (KeysParent.childCount > 0)
            {
                Debug.LogError("Keys parent must not have any children");
                return;
            }
            int rowIndex = 0;
            Vector3 nextKeyPossition = KeysParent.position;
            foreach (string row in Rows)
            {
                // new row
                Transform rowParent = new GameObject("Row" + rowIndex).transform;
                rowParent.parent = KeysParent;
                // each row starts at zero
                nextKeyPossition.x = KeysParent.position.x;
                foreach (char c in row)
                {
                    // new key
                    GameObject keyGO = PrefabUtility.InstantiatePrefab(KeyPrefab, rowParent) as GameObject;
                    keyGO.transform.position = nextKeyPossition;
                    keyGO.name += c;
                    Key key = keyGO.GetComponent<Key>();
                    if (!key)
                    {
                        Debug.LogError("Key prefab must contain the key component.");
                        return;
                    }
                    // set key text
                    key.Text = c.ToString();
                    // move the next key by x offset and size of the prefab
                    nextKeyPossition.x += (xOffset + KeyPrefab.transform.localScale.x);
                }
                // increment row index
                rowIndex++;
                // move the next row by z offset and size of the key prefab
                nextKeyPossition.z -= (zOffset + KeyPrefab.transform.localScale.z);
            }
        }
#endif
    }
}