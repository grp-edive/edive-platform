// Created by Vojtech Bruza
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace EdiveEditor
{
    /// <summary>
    /// Allows to conveniently switch scenes in the editor.
    /// Based on https://rumorgames.com/fast-scene-switching-in-the-unity-editor/
    /// </summary>
    public class SceneSwitchWindow : EditorWindow
    {
        private Vector2 scrollPos;
        [MenuItem("Edive/Scene Switch Window...")]
        internal static void Init()
        {
            var window = (SceneSwitchWindow)GetWindow(typeof(SceneSwitchWindow), false, "Scene Switch");
            window.position = new Rect(window.position.xMin + 100f, window.position.yMin + 100f, 200f, 400f);
        }

        internal void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            this.scrollPos = EditorGUILayout.BeginScrollView(this.scrollPos, false, false);

            GUILayout.Label("Scenes In Build", EditorStyles.boldLabel);
            for (var i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                var scene = EditorBuildSettings.scenes[i];
                var sceneName = Path.GetFileNameWithoutExtension(scene.path);
                if (string.IsNullOrEmpty(sceneName) || !File.Exists(scene.path)) continue; // skip not found scenes
                var pressed = GUILayout.Button(i + ": " + sceneName, new GUIStyle(GUI.skin.GetStyle("Button")) { alignment = TextAnchor.MiddleLeft });
                if (pressed)
                {
                    if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                    {
                        EditorSceneManager.OpenScene(scene.path);
                    }
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }
    }
}