﻿using UnityEngine;
using UnityEditor;
using UnityEngine.XR.Interaction.Toolkit;

namespace EdiveEditor
{
    public class FindTeleportationAreas : EditorWindow
    {
        private Vector2 scrollPos;

        [MenuItem("Edive/Legacy/Find Objects with Teleportation Area script")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(FindTeleportationAreas));
        }

        public void OnGUI()
        {
            TeleportationArea[] tas = null;
            if (GUILayout.Button("Find object with TA component"))
            {
                tas = Resources.FindObjectsOfTypeAll<TeleportationArea>();
                Debug.Log("found: " + tas.Length + " objects.");
            }

            EditorGUILayout.BeginHorizontal();
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            if (tas != null)
            {
                foreach (var obj in tas)
                {
                    EditorGUILayout.ObjectField(obj, typeof(GameObject), true);
                    Debug.Log("     object: " + obj.name);
                }
            }
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndHorizontal();

        }

    }
}