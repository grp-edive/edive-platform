// Created by Vojtech Bruza
using UnityEngine;
using UVRN.Helpers;

// TODO can be probably moved to the given scenario
namespace Edive.Interactions
{
    [RequireComponent(typeof(ScaleTransform), typeof(UVRN_NetworkTransformSyncScale))]
    public class ScaleTransformToggle : MonoBehaviour
    {
        private ScaleTransform scaleTransform;
        private UVRN_NetworkTransformSyncScale scaleSync;

        public ScaleTransform.ScaleState State => scaleTransform.CurrentState;

        private void Awake()
        {
            scaleTransform = GetComponent<ScaleTransform>();
            scaleSync = GetComponent<UVRN_NetworkTransformSyncScale>();
        }

        public void ToggleMinimized()
        {
            ToggleState(ScaleTransform.ScaleState.Minimized);
        }

        public void ToggleMaximized()
        {
            ToggleState(ScaleTransform.ScaleState.Maximized);
        }

        /// <summary>
        /// Toggle back and forth between given state and normal state (and sync)
        /// </summary>
        /// <param name="state"></param>
        public void ToggleState(ScaleTransform.ScaleState state)
        {
            if (scaleTransform.CurrentState != state)
            {
                SetState(state);
            }
            else
            {
                SetState(ScaleTransform.ScaleState.Normal);
            }
            
        }

        /// <summary>
        /// Set state and sync
        /// </summary>
        /// <param name="state"></param>
        public void SetState(ScaleTransform.ScaleState state)
        {
            scaleTransform.CurrentState = state;
            scaleSync.Cmd_SetScale(transform.localScale);
        }
    }
}
