// Created by Vojtech Bruza
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
using UVRN.Player;

// TODO move to MUVRE/UVRN or somewhere?
// Based on https://www.youtube.com/watch?v=cxRnK8aIUSI
namespace Edive.Interactions.Locomotion
{
    public class TeleportationManager : MonoBehaviour
    {
        [SerializeField]
        private XRRayInteractor xrRayInteractor;

        [SerializeField]
        private new Transform camera;

        [SerializeField]
        private TeleportationProvider teleportationProvider;

        [SerializeField]
        private InputActionReference selectInputAction;

        [SerializeField]
        private InputActionReference activateInputAction;

        [SerializeField]
        private InputActionReference cancelInputAction;

        [SerializeField]
        private InputActionReference thumbstickInputAction;

        [SerializeField]
        private bool applyRotationAfterTeleport = true;

        [SerializeField]
        private float rotationVisDistance = 0.25f;

        private Transform rotationVisualization;

        private bool _isSelectActive;
        private bool _isTeleportActive;
        private InputAction _thumbstick;
        private Vector2 _thumbstickDirection;

        private void Start()
        {
            // rotation visualization TODO move to separate script that just reads the value from here and sets some nicer object rotation
            rotationVisualization = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
            rotationVisualization.gameObject.name = "[XR]TeleportRotationVis";
            rotationVisualization.localScale *= 0.05f;
            rotationVisualization.gameObject.SetActive(false);

            DisableTeleport();

            // TODO fix these actions - activate should be select...but when activate happens? (joystick vs primary button)
            // TODO use started/performed/canceled
            var select = selectInputAction.action;
            select.Enable();
            select.performed += OnTeleportSelect;

            var activate = activateInputAction.action;
            activate.Enable();
            activate.performed += OnTeleportActivate;

            var cancel = cancelInputAction.action;
            cancel.Enable();
            cancel.performed += OnTeleportCancel;

            _thumbstick = thumbstickInputAction.action;
            _thumbstick.Enable();
        }

        private void Update()
        {
            // TODO rewrite as coroutine?

            if (!_isTeleportActive || _isSelectActive)
            {
                return;
            }

            RaycastHit hit;
            bool validTarget = xrRayInteractor.TryGetCurrent3DRaycastHit(out hit);
            var rotationAngle = applyRotationAfterTeleport ? CalculateTeleportRotationAngle() : 0;

            if (_thumbstick.triggered)
            {
                var thumbstickValue = _thumbstick.ReadValue<Vector2>();
                // if the thumbstick is pushed far enough in any direction we'll
                // save it off for use in the rotation angle calculation.
                if (thumbstickValue.sqrMagnitude >= 1)
                {
                    _thumbstickDirection = thumbstickValue.normalized;
                }

                if (applyRotationAfterTeleport)
                {
                    var rot_ang = rotationAngle * Mathf.Deg2Rad;
                    rotationVisualization.position
                    = new Vector3(hit.point.x + Mathf.Sin(rot_ang) * rotationVisDistance,
                                  hit.point.y,
                                  hit.point.z + Mathf.Cos(rot_ang) * rotationVisDistance);
                }
                return;
            }


            if (!validTarget)
            {
                DisableTeleport();
                return;
            }


            var request = new TeleportRequest()
            {
                destinationPosition = hit.point,
                destinationRotation = Quaternion.Euler(0,
                    rotationAngle, 0),
                matchOrientation = MatchOrientation.TargetUpAndForward
            };

            teleportationProvider.QueueTeleportRequest(request);
            DisableTeleport();
        }

        private void OnTeleportSelect(InputAction.CallbackContext callbackContext)
        {
            _isSelectActive = true;
            EnableTeleport();
        }
        private void OnTeleportActivate(InputAction.CallbackContext callbackContext)
        {
            _isSelectActive = false;
            EnableTeleport();
        }

        private void EnableTeleport()
        {
            xrRayInteractor.enabled = true;
            _isTeleportActive = true;
            rotationVisualization.gameObject.SetActive(applyRotationAfterTeleport);
        }

        private void OnTeleportCancel(InputAction.CallbackContext callbackContext)
        {
            DisableTeleport();
        }

        private void DisableTeleport()
        {
            xrRayInteractor.enabled = false;
            _isTeleportActive = false;
            _thumbstickDirection = Vector2.zero;
            rotationVisualization.gameObject.SetActive(false);
        }

        /// <summary>
        /// Calculates the angle by looking at the thumbstick direction, converting
        /// it into an angle, grabbing the camera's Y angle, and adding those two
        /// together.
        /// </summary>
        /// <returns>An angle in degrees as a float.</returns>
        private float CalculateTeleportRotationAngle()
        {
            var thumbstickAngle = Mathf.Atan2(_thumbstickDirection.x,
                                      _thumbstickDirection.y)
                                  * Mathf.Rad2Deg;
            return thumbstickAngle + camera.eulerAngles.y;
        }

    }
}
