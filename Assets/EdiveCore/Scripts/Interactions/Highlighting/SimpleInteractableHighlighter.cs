﻿// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Edive.Interactions.Highlighting
{
    [RequireComponent(typeof(XRSimpleInteractable))]
    public class SimpleInteractableHighlighter : MonoBehaviour
    {
        [SerializeField]
        private Renderer rend;

        private XRSimpleInteractable simpleInteractable;

        [Tooltip("If null, use the default that is on this gameobject.")]
        public Material IdleMaterial;
        [Tooltip("If null, use the default that is on this gameobject.")]
        public Material HoveredMaterial;
        [Tooltip("If null, use the default that is on this gameobject.")]
        public Material SelectedMaterial;
        [Tooltip("If null, use the default that is on this gameobject.")]
        public Material ActivatedMaterial;

        [Tooltip("Locks the current material and prevent any change from this component.")]
        public bool LockMaterial = false;

        private void Awake()
        {
            if (!rend) rend = GetComponent<Renderer>();
            simpleInteractable = GetComponent<XRSimpleInteractable>();
        }

        private void Start()
        {
            if (!IdleMaterial) IdleMaterial = rend.material;
            if (!HoveredMaterial) HoveredMaterial = rend.material;
            if (!SelectedMaterial) SelectedMaterial = rend.material;
            if (!ActivatedMaterial) ActivatedMaterial = rend.material;
        }

        private void OnEnable()
        {
            simpleInteractable.firstHoverEntered.AddListener(OnHoverEnter);
            simpleInteractable.selectEntered.AddListener(OnSelectEnter);
            simpleInteractable.activated.AddListener(OnActivated);
            simpleInteractable.deactivated.AddListener(OnDeactivated);
            simpleInteractable.selectExited.AddListener(OnSelectExit);
            simpleInteractable.lastHoverExited.AddListener(OnHoverExit);
        }
        private void OnDisable()
        {
            simpleInteractable.firstHoverEntered.AddListener(OnHoverEnter);
            simpleInteractable.selectEntered.AddListener(OnSelectEnter);
            simpleInteractable.activated.AddListener(OnActivated);
            simpleInteractable.deactivated.AddListener(OnDeactivated);
            simpleInteractable.selectExited.AddListener(OnSelectExit);
            simpleInteractable.lastHoverExited.AddListener(OnHoverExit);
        }

        private void OnHoverEnter(HoverEnterEventArgs arg0)
        {
            if (!LockMaterial) rend.material = HoveredMaterial;
        }

        private void OnSelectEnter(SelectEnterEventArgs args)
        {
            if (!LockMaterial) rend.material = SelectedMaterial;
        }

        private void OnActivated(ActivateEventArgs arg0)
        {
            if (!LockMaterial) rend.material = ActivatedMaterial;
        }

        private void OnDeactivated(DeactivateEventArgs arg0)
        {
            if (!LockMaterial) rend.material = SelectedMaterial;
        }

        private void OnSelectExit(SelectExitEventArgs arg0)
        {
            if (!LockMaterial) rend.material = HoveredMaterial;
        }

        private void OnHoverExit(HoverExitEventArgs arg0)
        {
            if (!LockMaterial) rend.material = IdleMaterial;
        }

        public void ToggleLock()
        {
            LockMaterial = !LockMaterial;
        }
    }
}
