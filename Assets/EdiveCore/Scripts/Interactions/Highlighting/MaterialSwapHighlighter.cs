﻿// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Interactions.Highlighting
{
    public class MaterialSwapHighlighter : MonoBehaviour
    {
        [SerializeField]
        private Renderer rend;

        [SerializeField]
        private Material HighlightMaterial;

        [Tooltip("If null, use the default that is on this gameobject.")]
        public Material IdleMaterial;

        [SerializeField]
        private Material DisabledMaterial;

        [SerializeField]
        private Material DisabledHighlightMaterial;


        private bool _enabled = true;
        private bool _highlighted = false;

        private Material _idleMaterial;

        private void Start()
        {
            if (rend == null)
            {
                Debug.LogError("Missing reference to Renderer component on GO: '" + gameObject.name + "'");
            }
            else if (!IdleMaterial)
            {
                IdleMaterial = rend.material;
            }
            else
            {
                rend.material = IdleMaterial;
            }
        }

        public void SetEnabled(bool buttonEnabled)
        {
            _enabled = buttonEnabled;

            //update state
            if (_highlighted)
            {
                Higlight();
            }
            else
            {
                Unhighlight();
            }
        }

        public void Higlight()
        {
            if (_enabled)
            {
                rend.material = HighlightMaterial;
            }
            else
            {
                rend.material = DisabledHighlightMaterial;
            }
        }

        public void Unhighlight()
        {
            if (_enabled)
            {
                rend.material = IdleMaterial;
            }
            else
            {
                rend.material = DisabledMaterial;
            }
        }
    }
}
