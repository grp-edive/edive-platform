// Created by Vojtech Bruza
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace Edive.Interactions.Transformations
{
    [RequireComponent(typeof(AdditionalTransformData))]
    public class DelayedObjectTransfromReset : MonoBehaviour
    {
        private AdditionalTransformData transformData;
        private IEnumerator delayedResetCoroutine;
        private List<DelayedObjectTransfromResetArea> resetAreas = new List<DelayedObjectTransfromResetArea>();

        [Tooltip("Will auto-start the delayed reset on trigger enter")]
        public bool autoStartReset = false;
        [SerializeField]
        [Tooltip("Do not auto-start reset, instead use the SelectExited on grab interactable. Will try to find this in awake, when autostart is not enabled.")]
        private XRGrabInteractable grabInteractable;
        [SerializeField]
        private bool useGrabInteractableEvents = false;
        [Tooltip("When not using the transform manager or something else to save the transforms")]
        public bool saveObjectTransformInStart = false;

        [Tooltip("Will be prioritized before saved transform in start. Will try to match this if not null when reseting.")]
        public MatchTransform overrideSavedTransform;

        public UnityEvent OnReset = new UnityEvent();

        private void Awake()
        {
            transformData = GetComponent<AdditionalTransformData>();
            if (!grabInteractable) grabInteractable = GetComponent<XRGrabInteractable>();
        }

        private void Start()
        {
            if (saveObjectTransformInStart) transformData.SaveTransform();
        }

        private void OnEnable()
        {
            if (useGrabInteractableEvents)
            {
                grabInteractable.selectExited.AddListener(OnSelectExit);
                grabInteractable.selectEntered.AddListener(OnSelectEnter);
            }
        }

        private void OnDisable()
        {
            if (useGrabInteractableEvents)
            {
                grabInteractable.selectExited.RemoveListener(OnSelectExit);
                grabInteractable.selectEntered.RemoveListener(OnSelectEnter);
            }
        }

        private void OnSelectExit(SelectExitEventArgs args)
        {
            TryStartReset();
        }

        private void OnSelectEnter(SelectEnterEventArgs args)
        {
            CancelDelayedReset();
        }

        public void CancelDelayedReset()
        {
            if (delayedResetCoroutine != null)
            {
                Debug.Log("Stopping delayed reset for " + gameObject.name);
                StopCoroutine(delayedResetCoroutine);
            }
            delayedResetCoroutine = null;
        }

        public void ForceStartDelayedReset(float delay)
        {
            // if there is already some coroutine, cancel it first
            CancelDelayedReset();
            Debug.Log("Starting delayed reset for " + gameObject.name);
            delayedResetCoroutine = DelayedReset(delay);
            StartCoroutine(delayedResetCoroutine);
        }

        private IEnumerator DelayedReset(float timeToWait)
        {
            yield return new WaitForSeconds(timeToWait);
            Debug.Log("Reseting misplaced object " + gameObject.name);
            if (overrideSavedTransform)
            {
                overrideSavedTransform.Match();
            }
            else transformData.ApplySavedTransforms(true, true, true);
            OnReset.Invoke();
        }

        private void OnTriggerEnter(Collider other)
        {
            var resetArea = other.gameObject.GetComponent<DelayedObjectTransfromResetArea>();
            if (resetArea)
            {
                resetAreas.Add(resetArea);
                if (resetAreas.Count == 1 && autoStartReset) // first reset area entered
                {
                    //Debug.Log($"Object {gameObject.name} entered {other.gameObject.name}. Starting delayed reset ({resetArea.WaitToResetTime}s).");
                    ForceStartDelayedReset(resetArea.WaitToResetTime);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            var resetArea = other.gameObject.GetComponent<DelayedObjectTransfromResetArea>();
            if (resetArea)
            {
                resetAreas.Remove(resetArea);
                if (resetAreas.Count == 0) // last reset area left
                {
                    //Debug.Log($"Object {gameObject.name} left {other.gameObject.name}. Stopping delayed reset.");
                    CancelDelayedReset();
                }
            }
        }

        public void TryStartReset()
        {
            if (autoStartReset) Debug.Log("This object is set to auto-start. Is this intentional?");
            if (resetAreas.Count > 0) ForceStartDelayedReset(resetAreas[0].WaitToResetTime);
        }
    }
}