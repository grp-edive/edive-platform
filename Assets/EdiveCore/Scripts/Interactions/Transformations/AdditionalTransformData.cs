// Created by Vojtech Bruza
using UnityEngine;

/// <summary>
/// TODO - add some comments
/// TODO - rename this class
/// Vojtech Bruza
/// </summary>

namespace Edive.Interactions.Transformations
{
    public class AdditionalTransformData : MonoBehaviour
    {
        [HideInInspector]
        public Vector3 savedPosition;
        [HideInInspector]
        public Quaternion savedRotation;
        [HideInInspector]
        public Vector3 savedLocalScale;

        public void SaveTransform()
        {
            savedPosition = transform.position;
            savedRotation = transform.rotation;
            savedLocalScale = transform.localScale;
        }

        // reset object to saved values
        public void ApplySavedTransforms(bool appplyPosition = true, bool applyRotation = true, bool applyScale = false)
        {
            if (appplyPosition) transform.position = savedPosition;
            if (applyRotation) transform.rotation = savedRotation;
            if (applyScale) transform.localScale = savedLocalScale;
        }

        // Palko...TODO move somewhere:
        public void MoveMetricCube(Vector3 v)
        {
            transform.position = new Vector3(transform.position.x + v.x, transform.position.y + v.y, transform.position.z + v.z);
            // transform.position.Set();
        }
    }
}
