// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Interactions.Transformations
{
    public class DelayedObjectTransfromResetArea : MonoBehaviour
    {
        [Tooltip("Time in seconds that the object waits before applying the reset.")]
        public float WaitToResetTime = 10;
    }
}