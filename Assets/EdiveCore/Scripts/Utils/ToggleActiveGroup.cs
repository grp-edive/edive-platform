// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Utils
{
    public class ToggleActiveGroup : MonoBehaviour
    {
        [SerializeField]
        private bool active = true;

        [SerializeField]
        private GameObject[] onWhenActive;
        [SerializeField]
        private GameObject[] offWhenActive;

        private void Start()
        {
            Toggle(active);
        }

        public void Toggle(bool activate)
        {
            active = activate;
            foreach(var go in onWhenActive) go.SetActive(active);
            foreach(var go in offWhenActive) go.SetActive(!active);
        }

        public void Toggle()
        {
            Toggle(!active);
        }
    }
}
