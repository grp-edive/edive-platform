// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Utils
{
    public class WorldGrid : MonoBehaviour
    {
        [Tooltip("X dimension range")]
        public Vector2 xRange = new Vector2(-10, 10);

        [Tooltip("Y dimension range")]
        public Vector2 yRange = new Vector2(0, 1);

        [Tooltip("Z dimension range")]
        public Vector2 zRange = new Vector2(-10, 10);

        [Tooltip("Spacing between elements for each dimension")]
        public Vector3 step = new Vector3(2, 2, 2);

        [Tooltip("Will use the primitive if null")]
        public GameObject elementPrefab;

        [Tooltip("Create primitive if the element prefab is null")]
        public PrimitiveType primitive = PrimitiveType.Sphere;

        public void Generate()
        {
            GameObject newGrid = new GameObject("Generated Grid");
            newGrid.transform.parent = transform;
            for (float x = xRange.x; x <= xRange.y; x += step.x)
            {
                for (float y = yRange.x; y <= yRange.y; y += step.y)
                {
                    for (float z = zRange.x; z <= zRange.y; z += step.z)
                    {
                        GameObject newElement;
                        if (elementPrefab)
                        {
                            newElement = Instantiate(elementPrefab, newGrid.transform);
                        }
                        else
                        {
                            newElement = GameObject.CreatePrimitive(primitive);
                            newElement.transform.parent = newGrid.transform;
                        }
                        newElement.name = $"{x}, {y}, {z}";
                        newElement.transform.position = new Vector3(x, y, z);
                    }
                }
            }
        }
    }
}
