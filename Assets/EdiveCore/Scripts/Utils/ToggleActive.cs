﻿// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Utils
{
    public class ToggleActive : MonoBehaviour
    {
        private bool activated = true;

        [SerializeField]
        private GameObject onWhenActive;
        [SerializeField]
        private GameObject offWhenActive;

        public void Toggle(bool active)
        {
            activated = active;
            if (onWhenActive) onWhenActive.SetActive(active);
            if (offWhenActive) offWhenActive.SetActive(!active);
        }

        public void Toggle()
        {
            Toggle(!activated);
        }
    }
}
