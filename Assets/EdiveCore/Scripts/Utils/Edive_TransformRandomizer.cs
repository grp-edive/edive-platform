// Created by Vojtech Bruza
using UnityEngine;

namespace Edive.Utils
{
    public class Edive_TransformRandomizer : MonoBehaviour
    {
        public Transform min;
        public Transform max;

        public bool randomizePosition = true;
        public bool randomizeRotation = false;
        public bool randomizeScale = false;

        public bool randomizeOnStart = true;

        private void Start()
        {
            if (randomizeOnStart) Randomize();
        }

        [ContextMenu("Randomize Transform")]
        public void Randomize()
        {
            if (randomizePosition) transform.position = Utilities.RandomVec3(min.position, max.position);
            if (randomizeRotation) transform.rotation = Quaternion.Euler(Utilities.RandomVec3(min.rotation.eulerAngles, max.rotation.eulerAngles));
            if (randomizeScale) transform.localScale = Utilities.RandomVec3(min.localScale, max.localScale);
        }

        [ContextMenu("Reset MinMax")]
        private void ResetMinMax()
        {
            // cannot be done in Reset, because the reference is already lost there
#if UNITY_EDITOR
            if (min) DestroyImmediate(min.gameObject);
            if (max) DestroyImmediate(max.gameObject);
#else
            if (min) Destroy(min.gameObject);
            if (max) Destroy(max.gameObject);
#endif

            CreateMinMax();
        }

        private void Reset()
        {
            CreateMinMax();
        }

        private void CreateMinMax()
        {
            min = new GameObject(gameObject.name + " Min Transform").transform;
            max = new GameObject(gameObject.name + " Max Transform").transform;

            min.parent = transform.parent;
            max.parent = transform.parent;

            transform.CopyTo(min);
            transform.CopyTo(max);
        }

        private void OnDrawGizmosSelected()
        {
            if (min && max)
            {
                var center = (min.position + max.position) / 2f;
                Gizmos.DrawWireCube(center, max.position - min.position);
            }
        }
    }
}
