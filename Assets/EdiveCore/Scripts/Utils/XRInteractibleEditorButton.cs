﻿// Created by Vojtech Bruza
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Edive.Utils
{
    // TODO remove this from build
    [ExecuteAlways]
    public class XRInteractibleEditorButton : MonoBehaviour
    {
        private XRSimpleInteractable xrSimpleInteractable;

        public void Reset()
        {
            if (xrSimpleInteractable == null)
            {
                xrSimpleInteractable = GetComponent<XRSimpleInteractable>();
            }
        }

        public void OnEnable()
        {
            Reset();
        }

        public void Invoke()
        {
            if (xrSimpleInteractable == null)
            {
                Debug.LogError("XRSimpleInteractable not initialized, try reseting the component...");
                return;
            }

            xrSimpleInteractable.firstHoverEntered.Invoke(new HoverEnterEventArgs());
            xrSimpleInteractable.hoverEntered.Invoke(new HoverEnterEventArgs());
            xrSimpleInteractable.selectEntered.Invoke(new SelectEnterEventArgs());
            xrSimpleInteractable.selectExited.Invoke(new SelectExitEventArgs());
            xrSimpleInteractable.hoverExited.Invoke(new HoverExitEventArgs());
            xrSimpleInteractable.lastHoverExited.Invoke(new HoverExitEventArgs());
        }
    }
}
