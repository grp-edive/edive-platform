// Created by Vojtech Bruza
using System.Collections.Generic;
using UnityEngine;
using UVRN.Player;

/// <summary>
/// Probably not needed to write it any different for now.
/// The default Mirror auth is the similar to (probably not better than) this authenticator.
/// The only difference is that it is not being called from internal Mirror code, but from our own...so there is probably no need to redo it now.
/// </summary>
public class Edive_PlayerManager : UVRN_PlayerManager
{
    public void LoadRoles(Edive.App.Settings.EdiveServerSettingsObject serverSettings)
    {
        var defaultRolesToFile = new Dictionary<string, string>();
        // go through all editor assigned roles and use them as file default. Will not be used when loading existing file (overriden by the file)
        foreach (var r in rolePasswordPairs)
        {
            defaultRolesToFile[r.role] = r.password;
        }
        if (serverSettings.LoadServerRolesFromFile(defaultRolesToFile))
        {
            var roles = new List<RolePasswordPair>();
            foreach (var r in serverSettings.Roles)
            {
                Debug.Log("Adding role " + r.Key + ":" + r.Value);
                roles.Add(new RolePasswordPair() { role = r.Key, password = r.Value });
            }
            rolePasswordPairs = roles;
            Debug.Log("Roles overriden by the file");
        }
        if (rolePasswordPairs.Count > 0)
        {
            requireRolePassword = true;
            Debug.Log("No roles required to enter the server");
        }
        else
        {
            requireRolePassword = false;
        }
    }
}
