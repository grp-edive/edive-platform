// Created by Vojtech Bruza
using Edive.UI;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace Edive.Networking
{
    public class SlideShowNetworking : NetworkBehaviour
    {
        public bool preloadOnClient = true;

        // logger event
        public UnityEvent<NetworkConnection, int> setSlideEvent = new UnityEvent<NetworkConnection, int>();

        [SerializeField]
        private SlideshowController slideshow;

        public override void OnStartClient()
        {
            Cmd_GetSlide();
        }

        [TargetRpc]
        private void TargetRpc_SetSlide(NetworkConnection conn, int slide)
        {
            slideshow.SetSlide(slide);
        }

        [ClientRpc]
        private void Rpc_SetSlide(int slide)
        {
            slideshow.SetSlide(slide);
        }

        #region Commands

        [Command(requiresAuthority = false)]
        private void Cmd_GetSlide(NetworkConnectionToClient conn = null)
        {
            // get info from server (for init)
            TargetRpc_SetSlide(conn, slideshow.activeSlideNumber);
        }

        [Command(requiresAuthority = false)]
        private void Cmd_NextSlide(NetworkConnectionToClient conn = null)
        {
            int oldSlide = slideshow.activeSlideNumber;
            slideshow.NextSlide();

            // if something changed
            if (oldSlide != slideshow.activeSlideNumber)
            {
                setSlideEvent.Invoke(conn, slideshow.activeSlideNumber);
                Rpc_SetSlide(slideshow.activeSlideNumber);
            }
        }

        [Command(requiresAuthority = false)]
        private void Cmd_PrevSlide(NetworkConnectionToClient conn = null)
        {
            int oldSlide = slideshow.activeSlideNumber;
            slideshow.PrevSlide();

            // if something changed
            if (oldSlide != slideshow.activeSlideNumber)
            {
                setSlideEvent.Invoke(conn, slideshow.activeSlideNumber);
                Rpc_SetSlide(slideshow.activeSlideNumber);
            }
        }

        [Client]
        public void NextSlide_CallCmd()
        {
            if (preloadOnClient) slideshow.NextSlide();
            Cmd_NextSlide();
        }

        [Client]
        public void PrevSlide_CallCmd()
        {
            if (preloadOnClient) slideshow.PrevSlide();
            Cmd_PrevSlide();
        }
        #endregion
    }
}
