If you want to configure server settings, just create a file called "server.txt" in "ServerSettings" folder in the root directory.
You can set string values: "port=", "server_title=",
Or bool values (0 or 1): "local_ip=", "auto_register_server="
You can check the source code for more info or contact the developer at bruza@mail.muni.cz


Desktop client controls:
Left click - world-space UI
Mute mic - M