# Edive Logging Guide

## Logging system

The logging system is composed of a main logger singleton (`UVRN_Logger` class), a generic logger (`UVRN_GenericLogger` class) and a number of specialized loggers.

![Logging System](../resources/LoggingSystem.png)

All components of the logging system are enclosed in `URVN.Logging` namespace - TODO-shouldn't `UVRN`??, and any future extensions should be too.

## Using a logger component

To log events and state changes of an object, navigate to `Assets/UVRN/Scripts/Logging/Logger Components` and find a corresponding logger component for the object (loggers are named after the script they listen to). Simply drag and drop the logger script onto the object.

Logger components have setting you may tweak to change how they log:

| Title | Description | Range | Default |
| ----- | ----------- | ----- | ------- |
| **Log frequency** | how many times per second will changes in transforms be logged | 0 - 60 | 24 |
| **Log on start** | start to log changes automatically (=true), or manually through the `StartLoggingRoutine()`  (=false) | true / false | true |
| **Log position / rotation / local scale** | log changes in position, rotation, scale. | true / false | true |
| **Log custom events** | log other, custom made events, generic logger does not have any. | true / false | true |

![Logger](../resources/logger.png)

If there is not a corresponding logger, you may use the generic one (`UVRN_GenericLogger`) to log transform changes, or create a new custom logger specific for your object.

## Creating a new custom logger
While implementing new functionality to the EDIVE project, consider logging information that may be important for reconstructing the virtual session. These especially include interactions between players, objects and environment, objects changing their state (position, …) and server events (players connecting and disconnecting, …).

### 1.  Identify what you want to log
Before creating a new custom logger, first identify the object and script which the logger will be listening to and what events and information you want to log.
<![endif]-->

### 2. Create a new script and class

In the `Assets/UVRN/Scripts/Logging/Logger Components` folder (path may change with time) create a new C# script. For easier identification, specialized loggers are named after the classes/scripts they are listening to (e.g. `UVRN_InteractibleObjectLogger` is listening to `InteractibleObject` class).

Create a new class deriving from `UVRN_GenericLogger`, enclose the class in `UVRN.Logging` namespace. Include `UnityEngine`, `Mirror` and other namespaces you mean to use.

Optionally, you can add a `[RequireComponent]` attribute to the class, which will make sure the specified component is present on the same game object and you do not need to check yourself.

```
using UnityEngine;
using Mirror;

namespace UVRN.Logging
{
	[RequireComponent(typeof(MyObject)]
	class UVRN_MyObjectLogger : UVRN_GenericLogger
	{
		// your code here
	}
}
```

>[!TODO]
> rewrite the rest