# Source Codes Documentation
How to document your source codes or assets.

## Source Codes
Document the code following the [C\# documentation](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/xmldoc/recommended-tags) syntax.
Every time that some new source codes are pushed to master branch, the new version of API documentation will be generated automagically.

>[!TIP]
>You can use `///` (above a method or class) and your IDE will generate a documentation template for you (at least MS Visual Studio and JetBrains Rider do this, notepad probably not ;-). 

You can add some specific articles - tutorials, descriptions, etc. for your source codes or modules. Documentation (including this file) is stored in `Documentation` folder in the root folder of project. We are using the [DocFX](https://dotnet.github.io/docfx/index.html) to translate raw markdown files into web pages. 

