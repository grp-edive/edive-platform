# eDIVE Architecture
The software design of eDIVE is based on modularity principle.
At the top level, the system is divided into two parts: "Core" and additional modules.
The Core contains functionality common to all scenarios, including:
* rendering of users' avatars,
* fundamental interactions (such as avatars movement or manipulation with objects),
* a network layer providing synchronization of VE for all users,
* audio management.

Additional module can be either "teaching module", encapsulating one (or more) teaching scenario or "extension module" encapsulating a set of specific functionality or data, usable by teaching modules.
* Teaching module typically contains all assets needed for specific scenario. This could include: 2D graphics, 3D models, sounds and source codes defining functionality specific to this teaching module. E. g., "Contour Lines" teaching module contains functions and GPU shaders allowing real-time terrain deformations and the correct rendering of contour lines even on deformed terrain. Beside the individual git repository, most modules comes also with additional data package, containing large data - such as spherical videos, 3D models of terrains, etc.
* Extension module can be though about as an SW library, providing either set of functions, set of assets, or both. E. g., the **ProtoGIS** extension module provides functions to automatically download requested data from GIS web services and pre-process those data into Unity `Terrain` game object. This extension module can be used in any scenario requiring real 3D terrain data.

This SW architecture simplifies the process of development of new modules as we are reusing the common functionality of the Core and extension modules. Furthermore, we can develop new specific functionalities without interfering with existing modules.

## Core Module
>[!TODO]
> from paper - network architecture, interactions, avatars, etc.

### Avatars

### Interactions

### Network layer - UVRN
This is our own **U**nity **V**irtual **R**eality **N**etworking ~~module~~ `namespace` that aggregates functionality related to networked VR.
UVRN uses following 3rd party assets:
* Mirror -- a high level networking library for Unity. See [mirror-networking.com](https://mirror-networking.com/) for details. Mirror is not included directly as part of source codes of eDIVE, but there is one-click integration available. See [Setup](Setup.md) for details.
* Vivox -- library for networked text+voice messaging. In scope of eDIVE, we are using it for voice-chat functionality.
  ~~3. MUVRE -- our tool called Masaryk University Virtual Reality Essentials (pronounced /ˈmuː.vər/) that is a collection of assets that extend the XR Interaction Toolkit.~~

### User Roles
eDIVE support users with different roles.
>[!TODO]
> from paper

Roles: teacher and student. Observer??
The settings can be managed using the `Edive_PlayerManager`. The names of roles and passwords (optional) can be changed using the `roles.txt` in the `ServerSettings` folder.

### Scenes
The **Core** module already contains several Unity scenes:
* The main scene - loaded as the first after application start. Allows to set the offline lobby scene as well as the online environment scene.
* TODO - lobby, screenshot
* TODO - new demo scene

Each teaching module contains at least one additional scene.
To set which scene should be loaded at begining of the scenario, use the `Online Scene` field in the `NetworkManager`, in `Main` scene. See [Setup](setup.md) for details. 


### Logging
We support that. See [Logging Guide](Logging.md) for details.

## Custom Unity Windows, Menus, etc.
To simplify the development process, we have implemented several custom windows in scope of Unity editor.

### Setup Window
Top menu: **Edive → Setup Window**. Serves for initial setup of the project. See [Setup](Setup.md) for more details.

### eDIVE Settings Window
Top menu: **Edive → Open Settings menu** to open the eDIVE settings tab. There, you can find the settings related to the application build
such as 'Build VR Client' button to open the build settings (Android platform). To build the server, use the automated build (Windows platform).

### Scene Switch
You can use the **Edive → Scene Switch Window** tab to quickly switch between scenes that are included in the build settings.


## Additional Features

### Relay Server
>[!TODO]
> We have a custom [server manager](http://decibel.fi.muni.cz/edive/) that can register and show running servers.
> It is used by the in-game lobby to show the currently running servers for the users.
> During the development please keep the IsDev server checkbox set to true (in editor or in the 'server.txt') to hide the development servers.


