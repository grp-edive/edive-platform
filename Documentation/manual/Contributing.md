**Authors of the original conception**: Čeněk Šašinka, Jiří Chmelík, Alžběta Šašinková, Zdeněk Stachoň.

# Software Development Contributions

## Core module
* Software design: Vojtěch Brůža and Jiří Chmelík
* Development: Vojtěch Brůža, Jiří Chmelík, Jonáš Rosecký 
* Additional content creation: Vojtěch Juránek, Vladimír Žbánek

## Teaching Modules
###  Contour Lines [not released yet]
* Authors of the scenario conception: Šašinka, Chmelík, Stachoň
* Instructional design: Stachoň, Šašinka. Šašinková, Švedová, Jochecová, Káčová
* Software design: Chmelík, Brůža   
* Development: Chmelík, Brůža
* Additional content creation: Stachoň, Kvarda, Chmelík, Brůža
* Testing: Šašinka, Stachoň, Švedová, Jochecová, Kvarda, Pelánová
* Implementation into praxes: Šašinka, Stachoň,  Švedová, Jochecová, Kvarda, Pelánová

---

# Funding
The eDIVE platform is developed in the research project of the Technology Agency of the Czech Republic (TL03000346 Education in Collaborative Immersive Virtual Reality) at the Department of Information and Library Studies and Department of Visual Computing, Masaryk University.

![logo TACR - technology agency of Czech Republic](../resources/logo_TACR_en_red.png)

--- 
