# Creating eDIVE builds - WIP

## Intro
eDIVE is based on client-server architecture. To work with the system, you need a client application and server application.

## Editor vs binaries
You can run either client or server directly from editor. Nevertheless, not both at the same time. 

## Platforms 
* server - standalone platform (Windows - tested), Linux and Mac should also work - not tested yet.
* client - primarily for VR HW - for Oculus Quest (our main target platform), it is Android platform.
  * btw - you will need to add "Android Build Support" module  to your Unity installation
  ![Android BUild](../resources/Building/Android-support.png)